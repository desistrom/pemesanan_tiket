<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login_verify
{
  function cek_login($id_user, $level)
  {
    if ($id_user) {
      if ($level == 1) {
        redirect(base_url('dashboard'));
      } else {
        redirect(base_url('home'));
      }
    }
  }

  function cek_admin($id_user, $level)
  {
    if ($id_user) {
      if ($level == 2) {
        redirect(base_url('home'));
      }
    } else {
      redirect(base_url('login'));
    }
  }
}
