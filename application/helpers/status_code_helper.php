<?php
if (!function_exists('status_code')) {
  function status_code($status_code, $transaksi_status, $status, $no_faktur)
  {
    $CI = get_instance();
    if ($status_code == '404') {
      return '<div class="badge badge-danger">belum melakukan pembayaran</div>';
    } else if ($status_code == '200') {

      if ($transaksi_status == 'settlement') {
        if ($status == '1' || $status == '0') {
          $CI->db->set('status', '2')
            ->where('no_faktur', $no_faktur)
            ->update('boking');
        }
        return '<div class="badge badge-success">transaksi success</div>';
      } else if ($transaksi_status == 'cancel') {
        if ($status != 0) {
          $CI->db->set('status', '0')
            ->where('no_faktur', $no_faktur)
            ->update('boking');
        }
        return '<div class="badge badge-danger">transaksi di batalkan</div>';
      }
      // batalkan transaksi
    } else if ($status_code === '201') {
      return '<span class="badge badge-warning">transaksi pending</span>';
    } else if ($status_code === '202') {
      if ($status != 0) {
        $CI->db->set('status', '0')
          ->where('no_faktur', $no_faktur)
          ->update('boking');
      }
      return '<span class="badge badge-danger">Sistem mendeteksi penipuan pembayaran</span>';

      // batalkan transaksi

    } else if ($status_code == '407') {
      if ($status != 0) {
        $CI->db->set('status', '0')
          ->where('no_faktur', $no_faktur)
          ->update('boking');
      }
      return '<span class="badge badge-danger">Transaksi kadaluarsa</span>';

      // batalkan trasnaksi

    } else {
      if ($status != 0) {
        $CI->db->set('status', '0')
          ->where('no_faktur', $no_faktur)
          ->update('boking');
      }
      return '<span class="badge badge-danger">Sistem error, tiket akan di batalkan</span>';

      // batalkan trasnaksi

    }
  }
}
