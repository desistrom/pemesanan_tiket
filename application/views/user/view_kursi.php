<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tampilan Kursi</title>
  <style>
    .box {
      padding: 4px;
      background-color: lightgreen;
      width: 50px;
      height: 50px;
      margin: 10px;
      float: left;
      border-radius: 5px;
    }

    .mb {
      margin-bottom: 40px
    }

    .mt {
      margin-top: 40px
    }
  </style>
</head>

<body>
  <?php for ($i = 100; $i < 200; $i++) { ?>
    <div class="box <?= $i >= 120 && $i < 130 ? 'mb' : '' ?> <?= $i >= 170 && $i < 180 ? 'mt' : '' ?>"><?= $i ?></div>
  <?php } ?>
</body>

</html>