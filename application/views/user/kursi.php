<!-- kabin satu -->
<?php
$openRow  = [0, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190];
$closeRow = [0, 109, 119, 129, 139, 149, 159, 169, 179, 189, 199];
$mb       = [0, 120];
$mt       = [0, 170];
$lainLain = [0, 139, 149, 159, 169]
?>
<div class="container mt-5">
  <div class="row">
    <div class="col-sm-12 border-bottom border-success">
      <h2 class="text-center text-success">Denah kursi untuk kapal <?= $kursi_1->row()->nama ?></h2>
    </div>
    <div class="col-sm-6 p-0">
      <button class="btn btn-primary m-3 text-white" type="button" id="btnSetTgl"><i class="fa fa-calendar"></i> Tentukan tanggal</button>
    </div>
  </div>
  <div>Keterangan :</div>
  <div style="overflow: hidden">
    <div class="float-left">
      <i class="fa fa-square text-success"></i> Kursi kosong
      <i class="fa ml-2 fa-square text-danger"></i> Kursi terboking
      <i class="fa ml-2 fa-square text-warning"></i> belum melakukan pembayaran
    </div>
    <div class="float-right">
      <i class="fa fa-calendar"></i> Tanggal : <?= $date ?>
    </div>
  </div>
</div>
</div>
<div class="container pb-2 mb-3 border rounded shadow">
  <h3 class="text-primary my-3 text-center border-bottom border-primary">kabin 1</h3>
  <?php foreach ($kursi_1->result() as $vk1) : ?>
    <?php
    $get_boking_status =  $model->get_boking_status($date, $vk1->id_kursi)->row();
    if ($get_boking_status) {
      $get_boking_status = $get_boking_status->status;
    }
    $status = $model->get_status($get_boking_status);
    ?>
    <?php if (array_search($vk1->kursi, $openRow)) : ?>
      <div class="row pl-2 <?= array_search($vk1->kursi, $mb) ? 'mb-5' : '' ?> <?= array_search($vk1->kursi, $mt) ? 'mt-5' : '' ?>">
      <?php endif ?>
      <div class="col-sm-1 p-0 <?= $status ?> rounded shadow text-white border text-center" style="margin:1px"><?= $vk1->kursi ?></div>
      <?php if ($vk1->kursi == '139' || $vk1->kursi == '169') : ?>
        <div class="col-sm-1 bg-success rounded shadow text-white border text-center"><img style="width: 20px" src="<?= base_url('src/jpeg/lamp.png') ?>" alt=""></i></div>
      <?php endif ?>
      <?php if ($vk1->kursi == '149' || $vk1->kursi == '159') : ?>
        <div class="col-sm-1 bg-success rounded shadow text-white border text-center"><img style="width: 20px" src="<?= base_url('src/jpeg/hydrant.png') ?>" alt=""></i></div>
      <?php endif ?>
      <?php if (array_search($vk1->kursi, $closeRow)) : ?>
      </div>
    <?php endif ?>
  <?php endforeach ?>
</div>
<!-- kabin 2 -->
<?php
$openRow  = [0, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290];
$closeRow = [0, 209, 219, 229, 239, 249, 259, 269, 279, 289, 299];
$mb       = [0, 220];
$mt       = [0, 270];
$lainLain = [0, 239, 249, 259, 269]
?>
<div class="container pb-2 mb-3 mt-5 border rounded shadow">
  <h3 class="text-primary my-3 text-center border-bottom border-primary">kabin 2</h3>
  <div class="mt-5">
    <?php foreach ($kursi_2->result() as $vk2) : ?>
      <?php
      $get_boking_status =  $model->get_boking_status($date, $vk2->id_kursi)->row();
      if ($get_boking_status) {
        $get_boking_status = $get_boking_status->status;
      }
      $status = $model->get_status($get_boking_status);
      ?>
      <?php if (array_search($vk2->kursi, $openRow)) : ?>
        <div class="row pl-2 <?= array_search($vk2->kursi, $mb) ? 'mb-5' : '' ?> <?= array_search($vk2->kursi, $mt) ? 'mt-5' : '' ?>">
        <?php endif ?>
        <div class="col-sm-1 p-0 <?= $status ?> rounded shadow text-white border text-center" style="margin:1px"><?= $vk2->kursi ?></div>
        <?php if ($vk2->kursi == '239' || $vk2->kursi == '269') : ?>
          <div class="col-sm-1 bg-success rounded shadow text-white border text-center"><img style="width: 20px" src="<?= base_url('src/jpeg/lamp.png') ?>" alt=""></i></div>
        <?php endif ?>
        <?php if ($vk2->kursi == '249' || $vk2->kursi == '259') : ?>
          <div class="col-sm-1 bg-success rounded shadow text-white border text-center"><img style="width: 20px" src="<?= base_url('src/jpeg/hydrant.png') ?>" alt=""></i></div>
        <?php endif ?>
        <?php if (array_search($vk2->kursi, $closeRow)) : ?>
        </div>
      <?php endif ?>
    <?php endforeach ?>
  </div>
</div>
<!-- kabin 3 -->
<?php
$openRow  = [0, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390];
$closeRow = [0, 309, 319, 329, 339, 349, 359, 369, 379, 389, 399];
$mb       = [0, 320];
$mt       = [0, 370];
$lainLain = [0, 339, 349, 359, 369]
?>
<div class="container pb-2 mb-3 mt-5 border rounded shadow">
  <h3 class="text-primary my-3 text-center border-bottom border-primary">kabin 3</h3>
  <div class="mt-5">
    <?php foreach ($kursi_3->result() as $vk3) : ?>
      <?php
      $get_boking_status =  $model->get_boking_status($date, $vk3->id_kursi)->row();
      if ($get_boking_status) {
        $get_boking_status = $get_boking_status->status;
      }
      $status = $model->get_status($get_boking_status);
      ?>
      <?php if (array_search($vk3->kursi, $openRow)) : ?>
        <div class="row pl-2 <?= array_search($vk3->kursi, $mb) ? 'mb-5' : '' ?> <?= array_search($vk3->kursi, $mt) ? 'mt-5' : '' ?>">
        <?php endif ?>
        <div class="col-sm-1 p-0 <?= $status ?> rounded shadow text-white border text-center" style="margin:1px"><?= $vk3->kursi ?></div>
        <?php if ($vk3->kursi == '339' || $vk3->kursi == '369') : ?>
          <div class="col-sm-1 bg-success rounded shadow text-white border text-center"><img style="width: 20px" src="<?= base_url('src/jpeg/lamp.png') ?>" alt=""></i></div>
        <?php endif ?>
        <?php if ($vk3->kursi == '349' || $vk3->kursi == '359') : ?>
          <div class="col-sm-1 bg-success rounded shadow text-white border text-center"><img style="width: 20px" src="<?= base_url('src/jpeg/hydrant.png') ?>" alt=""></i></div>
        <?php endif ?>
        <?php if (array_search($vk3->kursi, $closeRow)) : ?>
        </div>
      <?php endif ?>

    <?php endforeach ?>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalKursi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tanggal pelayaran dan kapal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= form_open(base_url('kursi'), 'autocomplate="false"'); ?>
      <div class="modal-body">
        <div class="form-group">
          <label for="id_kapal">Kapal</label>
          <select name="id_kapal" id="id_kapal" class="form-control">
            <option value="">Pilih kapal</option>
            <?php foreach ($kapal->result() as $vk) : ?>
              <option <?= $vk->id_kapal == $kursi_1->row()->id_kapal ? 'selected' : '' ?> value="<?= $vk->id_kapal ?>"><?= $vk->nama ?></option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="form-group">
          <label for="file">Tanggal Pelayaran</label>
          <input type="text" autocomplete="off" name="tgl" id="tgl" class="form-control" placeholder="Masukkan tanggal">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      <?= form_close() ?>
    </div>
  </div>
</div>