<?php $user = $user->row(); ?>
<div class="container mt-5">

  <div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
      <div class="card">
        <div class="card-body">
          <center class="m-t-30"> <img src="<?= base_url('assets/user/') . $user->foto ?>" class="rounded-circle" width="150" />
            <h4 class="card-title m-t-10"><?= $user->nama ?></h4>
            <h6 class="card-subtitle">Pemesanan tiket online </h6>
          </center>
        </div>
        <div>
          <hr>
        </div>
        <div class="card-body">
          <small class="text-muted">Email address </small>
          <h6><?= $user->email ?></h6>
          <small class="text-muted p-t-30 db">Alamat</small>
          <h6><?= $user->alamat ?></h6>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-xlg-9 col-md-7">
      <div class="card">
        <div class="card-body">
          <?= form_open(base_url('user/Akun/update_user'), 'class="form-horizontal form-material" id="formAkun"') ?>
          <input type="hidden" name="id" value="<?= $user->id_user ?>">
          <div class="form-group">
            <label for="disabledTextInput" class="col-md-12">Email</label>
            <div class="col-md-12">
              <input type="email" disabled name="email" value="<?= $user->email ?>" placeholder="Masukkan Email" class="form-control form-control-line text-secondary">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">Nama</label>
            <div class="col-md-12">
              <input id="nama" type="text" name="nama" value="<?= $user->nama ?>" placeholder="Masukkan nama" class="form-control form-control-line ">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">Alamat</label>
            <div class="col-md-12">
              <textarea rows="5" id="alamat" name="alamat" placeholder="ALamat lengkap" class="form-control form-control-line"><?= $user->alamat ?></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">Tanggal lahir</label>
            <div class="col-md-12">
              <input type="text" value="<?= $user->tgl_lahir ?>" name="tgl_lahir" id="tanggal" placeholder="masukkan tanggal lahir" class="form-control form-control-line">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">Genre</label>
            <div class="col-md-12">
              <select name="genre" id="genre" class="form-control form-control-line">
                <option value="">Pilih genre</option>
                <option <?= $user->genre == 'pria' ? 'selected' : '' ?> value="pria">Pria</option>
                <option <?= $user->genre == 'wanita' ? 'selected' : '' ?> value="wanita">Wanita</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">Password</label>
            <div class="col-md-12">
              <input type="password" name="password" placeholder="Masukkan password" class="form-control form-control-line">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <button class="btn btn-success text-white">Update Profile</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>