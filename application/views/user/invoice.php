<?php
$invc = $invoice->row();
$invoices = $invoice->result();
?>

<div class="container">
  <div class="card my-5">
    <div class="card-header bg-white">
      <div class="float-left">
        <h4 class="text-primary mb-1">Pelayaran group</h4>
        <p class="mt-0">pemesanan tiket kapal online </p>
      </div>
      <div class="float-right">
        <h5 class="text-success text-right mb-0">invoice</h5>
        <p class="text-right mt-0" style="width: 200px">Jl.manga dua jajar sukosewu</p>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="text-black col-lg-7 col-sm-12 p-3">
          <div class="font-weight-bold">Invoice To</div>
          <h5 class="mb-0"><?= $invc->nama ?></h5>
          <div id="idInvoiceBoking" class="d-none"><?= $invc->id_boking ?></div>
          <div><?= $invc->email ?></div>
          <div><?= $invc->alamat ? $invc->alamat : '-' ?></div>
        </div>
        <div class="text-black col-lg-5 col-sm-12 pb-2" style="width: 250px">
          <div class="mb-2 text-right">
            <img src="<?= base_url('user/Invoice/barcode/' . $invc->no_faktur) ?>" alt="">
          </div>
          <table>
            <tr>
              <td class="font-weight-bold">No faktur</td>
              <td>:</td>
              <td id="idFaktur"><?= $invc->no_faktur ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Status</td>
              <td>:</td>
              <td id="status">
                <?= status_code(
                  $response['status_code'],
                  $response['status_code'] == '500' || $response['status_code'] == '404' ? 'error' : $response['transaction_status'],
                  $invc->status,
                  $invc->no_faktur
                );
                ?>
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">Tanggal</td>
              <td>:</td>
              <td><?= $invc->tgl_boking ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Rute</td>
              <td>:</td>
              <td><?= $invc->tempat . '-' . $invc->tujuan ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Kapal</td>
              <td>:</td>
              <td><?= $invc->nama_kapal ?></td>
            </tr>
            <tr>
              <td class="font-weight-bold">Total (IDR)</td>
              <td>:</td>
              <td><?= 'Rp.' . number_format($invc->total) ?></td>
            </tr>
            <?php if ($response['status_code'] != '500') { ?>
              <?php if ($response['status_code'] != '404') { ?>
                <tr>
                  <td class="font-weight-bold">Tipe pembayaran</td>
                  <td>:</td>
                  <td id="idFaktur"><?= $response['store'] ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold">Code payment</td>
                  <td>:</td>
                  <td id="idFaktur"><?= $response['payment_code'] ?></td>
                </tr>
              <?php } ?>
            <?php } ?>
          </table>
        </div>
      </div>
      <div style="clear: both">
        <div class="border-top border-black mb-2"></div>
        <div class=" text-black bg-light p-3 rounded">
          <div class="table-responsive">
            <table class="table  w-100">
              <thead class="bg-primary text-white">
                <tr>
                  <th>Nama</th>
                  <th>Usia</th>
                  <th class="text-center">No Kabin</th>
                  <th class="text-center">No kursi</th>
                  <th>Waktu /tanggal</th>
                  <th>Phone</th>
                </tr>

              </thead>

              <tbody class="bg-white">
                <?php foreach ($invoices as $inv) : ?>
                  <tr>
                    <td><?= $inv->nama_penerima ?></td>
                    <td><?= $inv->usia ?></td>
                    <td class="text-center"><?= $inv->kabin ?></td>
                    <td class="text-center"><?= $inv->kursi ?></td>
                    <td><?= $inv->tgl_pelayaran ?></td>
                    <td><?= $inv->phone ?></td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>

          </div>
        </div>
        <div class="border-top border-black mb-2"></div>
      </div>
      <?php if ($this->session->userdata('id_user')) : ?>

        <?php if ($response['status_code'] == '404') : ?>
          <div id="containerKonfirmasi">
            <button class="btn btn-danger text-white" id="batalBoking">Batal Boking </button>
            <a href="<?= base_url('invoice/payment/') . $invc->no_faktur; ?>" class="btn btn-success text-white">Bayar sekarang</a>
            <div class="text-black font-weight-bold">(Silakan melakuakn pembayaran untuk melanjutkan transaksi)</div>
          </div>
        <?php endif ?>

        <?php if ($response['status_code'] == '500' || $response['status_code'] == '201') : ?>
          <div id="containerKonfirmasi">
            <a href="<?= base_url('user/Invoice/batal_transaksi/' . $invc->no_faktur) ?>" class="btn btn-danger text-white" id="batalBoking">Batal Boking </a>
          </div>
        <?php endif ?>

      <?php endif ?>
    </div>
  </div>
</div>
</div>