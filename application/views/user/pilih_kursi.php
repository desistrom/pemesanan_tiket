<?php
$openRow  = [0, $id_kabin . '00', $id_kabin . '10', $id_kabin . '20', $id_kabin . '30', $id_kabin . '40', $id_kabin . '50', $id_kabin . '60', $id_kabin . '70', $id_kabin . '80', $id_kabin . '90'];
$closeRow = [0, $id_kabin . '09', $id_kabin . '19', $id_kabin . '29', $id_kabin . '39', $id_kabin . '49', $id_kabin . '59', $id_kabin . '69', $id_kabin . '79', $id_kabin . '89', $id_kabin . '99'];
$mb       = [0, $id_kabin . '20'];
$mt       = [0, $id_kabin . '70'];
$lainLain = [0, $id_kabin . '39', $id_kabin . '49', $id_kabin . '59', $id_kabin . '69'];
$kursi_terpilih = [0];
$rute     = $this->db->get_where('rute', ['id_rute' => $id_rute])->row();
?>
<div class="container mt-5">
  <div class="row">
    <?= form_open(base_url('user/Boking/boking_tiket'), 'id="formBoking" class="col-sm-12"') ?>
    <div class="row">
      <div class="col-lg-6 data">
        <input type="hidden" name="id_rute" id="id_rute" value="<?= $id_rute ?>">
        <input type="hidden" name="id_kapal" id="id_rute" value="<?= $id_kapal ?>">
        <input type="hidden" name="tgl" value="<?= $tgl ?>">
        <input type="hidden" name="total" id="total" value="">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" disabled value="<?= $user->nama ?>" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
          <small id="errorNama" class="invalid-feedback"></small>
        </div>
        <div class="form-group">
          <label for="alamat">Alamat</label>
          <textarea name="alamat" disabled class="form-control" id="alamat" placeholder="Masukkan alamat"><?= $user->alamat ?></textarea>
          <small id="errorTgl" class="invalid-feedback"></small>
        </div>
        <div class="form-group">
          <label for="tgl">Tanggal</label>
          <input type="text" disabled value="<?= $tgl ?>" class="form-control" name="tanggal" id="tgl" placeholder="Masukkan tanggal">
          <small id="errorTgl" class="invalid-feedback"></small>
        </div>
        <div class="form-group">
          <label for="rute">Rute</label>
          <input type="text" disabled value="<?= $rute->tempat . '-' . $rute->tujuan ?>" class="form-control" name="rute" id="rute" placeholder="Masukkan rute">
          <small id="errorRute" class="invalid-feedback"></small>
        </div>

      </div>
      <div class="col-lg-6 data">

        <div class="form-group">
          <label for="tgl">Kursi</label>
          <ul class="list-group" id="listKursi">
            <?php foreach ($kursi_otomatis as $vko) : ?>
              <?php $kursi_terpilih[] = $vko->kursi ?>
              <li class="list-group-item justify-content-between align-items-center text-black">
                <div class="form-group">
                  <input type="text" name="nama[]" required placeholder="Nama Penerima" class="form-control">
                </div>
                <div class="form-group">
                  <input type="number" name="phone[]" required placeholder="Phone" class="form-control">
                </div>
                <div class="form-group d-flex justify-content-between">
                  No: <?= $vko->kursi ?>
                  <select required name="usia[]" id="<?= $vko->id_kursi ?>" class="badge badge-primary kursi-boking badge-pill">
                    <option value="">Pilih usia</option>
                    <option value="dewasa">Dewasa</option>
                    <option value="anak">Anak</option>
                  </select>
                </div>
              </li>
            <?php endforeach ?>
            <li class="list-group-item text-center " id="btnTambahKursi" style="cursor: pointer;">Tambah kursi <i class="fa fa-plus-circle"></i></li>
          </ul>
        </div>
        <div id="viewTarif">Total :</div>
        <button type="submit" class="btn btn-primary btn-lg text-white mb-3">Boking </button>

      </div>
      <div class="col-lg-12" style="display: none;" id="selectKursi">
        <div class="my-2" style="overflow: hidden">
          <div>Keterangan :</div>
          <div class="float-left">
            <i class="fa fa-square text-success"></i> Kursi kosong
            <i class="fa ml-2 fa-square" style="color:#bb6b0b"></i> kursi yang anda pilih
            <i class="fa ml-2 fa-square text-danger"></i> Kursi terboking
            <i class="fa ml-2 fa-square text-warning"></i> belum melakukan pembayaran

          </div>
        </div>
        <?php foreach ($kursi_1->result() as $vk1) : ?>
          <?php
          $get_boking_status =  $model->get_boking_status($date_only, $vk1->id_kursi)->row();
          if ($get_boking_status) {
            $get_boking_status = $get_boking_status->status;
          }
          $status = $model->get_status($get_boking_status);
          ?>
          <?php if (array_search($vk1->kursi, $openRow)) : ?>
            <div class="row pl-2 <?= array_search($vk1->kursi, $mb) ? 'mb-5' : '' ?> <?= array_search($vk1->kursi, $mt) ? 'mt-5' : '' ?>">
            <?php endif ?>
            <div id="ck-button" class="col-sm-1 p-0 <?= $status ?> text-white" style="margin:1px">
              <label class="checkbox-kursi mb-0">
                <input type="checkbox" <?= $get_boking_status == 2 || $get_boking_status == 1 ? 'disabled' : '' ?> <?= array_search($vk1->kursi, $kursi_terpilih) ? 'checked' : '' ?> name="id_kursi[]" class="d-none" no-kursi="<?= $vk1->kursi ?>" value="<?= $vk1->id_kursi ?>"><span><?= $vk1->kursi ?></span>
              </label>
            </div>
            <?php if ($vk1->kursi == $id_kabin . '39' || $vk1->kursi == $id_kabin . '69') : ?>
              <div class="col-sm-1 bg-success rounded shadow text-white border text-center"><img style="width: 20px" src="<?= base_url('src/jpeg/lamp.png') ?>" alt=""></i></div>
            <?php endif ?>
            <?php if ($vk1->kursi == $id_kabin . '49' || $vk1->kursi == $id_kabin . '59') : ?>
              <div class="col-sm-1 bg-success rounded shadow text-white border text-center"><img style="width: 20px" src="<?= base_url('src/jpeg/hydrant.png') ?>" alt=""></i></div>
            <?php endif ?>
            <?php if (array_search($vk1->kursi, $closeRow)) : ?>
            </div>
          <?php endif ?>
        <?php endforeach ?>
        <div class="mt-1 mb-2 clear-float border-top mt-1 pt-3">
          <button type="button" id="btnSaveKursi" class="btn btn-primary btn-lg text-white mr-3 float-left">Pilih kursi</button>
          <button type="button" class="btn btn-secondary btn-lg float-left" id="btnCancel">cancel</button>
        </div>
      </div>

    </div>
    <?= form_close() ?>
  </div>
</div>