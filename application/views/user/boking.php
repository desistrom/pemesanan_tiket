<div class="container mt-5">
  <input type="hidden" id="alert" value="<?= $this->session->flashdata('msg'); ?>">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6 mb-3">
          <img src="<?= base_url('src/jpeg/kapal.jpg') ?>" alt="" class="img-thumbnail">
        </div>
        <div class="col-lg-6">
          <div class="bg-white">
            <h3>Pemesanan tiket</h3>
            <?= form_open(base_url('pilih-kursi'), 'autocomplete="false"') ?>
            <div class="form-group">
              <label for="tanggal">Tanggal</label>
              <input autocomplete="off" type="text" name="tgl" id="tanggal" placeholder="Masukkan tanggal" class="form-control">
              <span class="invalid-feedback" id="errorTanggal"></span>
            </div>
            <div class="form-group">
              <label for="rute">Rute</label>
              <select class="form-control form-sm" name="id_rute" id="rute">
                <option value="">Pilih rute</option>
                <?php foreach ($rute->result() as $vr) : ?>
                  <option value="<?= $vr->id_rute ?>"><?= $vr->tempat ?> - <?= $vr->tujuan ?> (<?= $vr->jarak . ' KM' ?>)</option>
                <?php endforeach ?>
              </select>
              <span class="invalid-feedback" id="errorRute"></span>
            </div>
            <div id="dateTime" class="form-group row" style="display: none">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="kapal">Kapal</label>
                  <select required name="id_kapal" id="kapal" class="form-control">
                  </select>
                  <span class="invalid-feedback" id="errorKapal"></span>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label for="kapal">Kabin</label>
                    <select required name="id_kabin" id="kabin" class="form-control">
                      <option value="">Pilih kabin</option>
                      <?php foreach ($kabin->result() as $vk) : ?>
                        <option value="<?= $vk->id_kabin ?>"><?= $vk->kabin ?></option>
                      <?php endforeach ?>
                    </select>
                    <span class="invalid-feedback" id="errorKabin"></span>

                  </div>
                  <div class="col-sm-6">
                    <label for="kapal">Banyak kursi</label>
                    <input type="number" name="banyak_kursi" max="6" placeholder="banyak kursi yang di pesan" required class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <button class="btn btn-primary btn-lg text-white float-right">Pilih kursi</button>
              </div>
            </div>
            <?= form_close() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>