<div class="ftco-blocks-cover-1">
  <div class="ftco-cover-1 overlay cover" style="height: 100vh; min-height: 600px; background-image: url('https://source.unsplash.com/pSyfecRCBQA/1920x780')">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <h1>Selamat datang di
            Pemesanan Tiket Online Kapal Penyebrangan PT Dharma Lautan Utama</h1>
          <p class="mb-5">Mudah cepat dan terpercaya</p>
          <?= form_open(base_url('user/Invoice/cek_faktur'), 'id="formInvoice"') ?>
          <div class="form-group mb-0 d-flex">
            <input type="number" id="noFaktur" name="no_faktur" class="form-control" placeholder="Lacak no faktur">
            <button type="submit" class="btn btn-primary text-white px-4">Lacak</button>
          </div>
          <div class="text-warning d-none" id="errorNoFaktur"></div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- END .ftco-cover-1 -->
  <div class="ftco-service-image-1 pb-5">
    <div class="container">
      <div class="owl-carousel owl-all">
        <div class="service text-center">
          <a href="#"><img src="<?= base_url('src/') ?>images/cargo_sea_small.jpg" alt="Image" class="img-fluid"></a>
          <div class="px-md-3">
            <h3><a href="#">Sea Freight</a></h3>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
          </div>
        </div>
        <div class="service text-center">
          <a href="#"><img src="<?= base_url('src/') ?>images/cargo_air_small.jpg" alt="Image" class="img-fluid"></a>
          <div class="px-md-3">
            <h3><a href="#">Air Freight</a></h3>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
          </div>
        </div>
        <div class="service text-center">
          <a href="#"><img src="<?= base_url('src/') ?>images/cargo_delivery_small.jpg" alt="Image" class="img-fluid"></a>
          <div class="px-md-3">
            <h3><a href="#">Package Forwarding</a></h3>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>