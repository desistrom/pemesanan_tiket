<div class="container mb-3">
  <div class="row">
    <div class="col-lg-12">
      <!-- jadwal hari senin -->
      <?php if ($hari->num_rows() > 0) : ?>
        <?php foreach ($hari->result() as $vh) : ?>
          <div class="card mb-3">
            <div class="card-header">
              <h5><?= $vh->hari ?></h5>
            </div>
            <div class="card-body">
              <table class="table table-striped table-sm">
                <thead class="bg-primary text-white">
                  <tr>
                    <th scope="col" class="text-center">No</th>
                    <th scope="col">Kapal</th>
                    <th scope="col">Tempat</th>
                    <th scope="col">Tujuan</th>
                    <th scope="col">Jarak</th>
                    <th scope="col" class="text-center">Hari</th>
                    <th scope="col" class="text-center">Jam</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $jadwal = $this->jadwal->get_jadwal($vh->hari); ?>
                  <?php $no = 1;
                  foreach ($jadwal->result() as $vj) : ?>
                    <tr>
                      <th scope="row" class="text-center"><?= $no ?></th>
                      <td><?= $vj->nama ?></td>
                      <td><?= $vj->tempat ?></td>
                      <td><?= $vj->tujuan ?></td>
                      <td><?= $vj->jarak . ' KM' ?></td>
                      <td class="text-center"><?= $vj->hari ?></td>
                      <td class="text-center"><?= $vj->jam ?></td>
                    </tr>
                  <?php $no++;
                  endforeach ?>

                </tbody>
              </table>
            </div>
          </div>
        <?php endforeach ?>
      <?php endif ?>
      <?php if ($hari->num_rows() < 1) : ?>
        <div class="card">
          <div class="card-header">
            <h3>Jadwal kosong !!</h3>
          </div>
          <div class="card-body font-weight-bold text-primary">Belum ada jadwal pelayaran, hubungi admin jika ada yang di tanyakan
          </div>
        </div>
      <?php endif ?>
    </div>
  </div>
</div>