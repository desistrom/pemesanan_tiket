<?php
$id_user = $this->session->userdata('id_user');
if ($id_user) {

    $user    = $this->db->get_where('user', ['id_user' => $id_user])->row();
    // mengubah nama =menjadi array
    $full_name    = explode(" ", $user->nama);
    // membagi nama
    $nama_depan      = isset($full_name[0]) ? $nama_depan = $full_name[0] : $nama_depan = null;
    $nama_belakang   = isset($full_name[1]) ? $nama_belakang = $full_name[1] : $nama_belakang = null;
}
?>
<div id="overlayer"></div>
<div id="loader2" class="loader2 d-none">
    <div class="lds-facebook">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<div class="loader">
    <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>

<div class="site-wrap" id="home-section">

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>


    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="#" class=""><span class="mr-2  icon-envelope-open-o"></span> <span class="d-none d-md-inline-block">info@yourdomain.com</span></a>
                    <span class="mx-md-2 d-inline-block"></span>
                    <a href="#" class=""><span class="mr-2  icon-phone"></span> <span class="d-none d-md-inline-block">1+ (234) 5678 9101</span></a>


                    <div class="float-right">
                        <?php if ($id_user) { ?>

                            <a href="<?= base_url('akun') ?>" class=""><span class=""> Halo , <?= $nama_belakang ? ucwords($nama_depan) . ucwords($nama_belakang) : ucwords($nama_depan) ?> |</span></a>
                            <a href="<?= base_url('Auth/logout') ?>" class=""></span> <span class="">Logout</span></a>
                        <?php } else { ?>
                            <a href="<?= base_url('Login') ?>" class=""><span class="mr-2"></span> <span class="">Login</span></a>
                            <span class="mx-md-2 d-inline-block"></span>
                            <a href="<?= base_url('Register') ?>" class=""><span class="mr-2"></span> <span class="">Register</span></a>
                        <?php } ?>

                    </div>

                </div>

            </div>

        </div>
    </div>