<footer class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-7">
						<h2 class="footer-heading mb-4">About Us</h2>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					</div>
					<div class="col-md-4 ml-auto">
						<h2 class="footer-heading mb-4">Features</h2>
						<ul class="list-unstyled">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Testimonials</a></li>
							<li><a href="#">Terms of Service</a></li>
							<li><a href="#">Privacy</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</div>

				</div>
			</div>
			<div class="col-md-4 ml-auto">

				<div class="mb-5">
					<h2 class="footer-heading mb-4">Subscribe to Newsletter</h2>
					<form action="#" method="post" class="footer-suscribe-form">
						<div class="input-group mb-3">
							<input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
							<div class="input-group-append">
								<button class="btn btn-primary text-white" type="button" id="button-addon2">Subscribe</button>
							</div>
						</div>
				</div>


				<h2 class="footer-heading mb-4">Follow Us</h2>
				<a href="#about-section" class="smoothscroll pl-0 pr-3"><span class="icon-facebook"></span></a>
				<a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
				<a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
				<a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
				</form>
			</div>
		</div>
		<div class="row pt-5 mt-5 text-center">
			<div class="col-md-12">
				<div class="border-top pt-5">
					<p class="copyright">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>
							document.write(new Date().getFullYear());
						</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</p>
				</div>
			</div>

		</div>
	</div>
</footer>
<script>
	let base_url = '<?= base_url() ?>';
</script>
<?php

$uri = strtolower($this->uri->segment(1));
?>
<script src="<?= base_url('src/') ?>js/jquery-3.3.1.min.js"></script>
<script src="<?= base_url('src/') ?>js/popper.min.js"></script>
<script src="<?= base_url('src/') ?>js/bootstrap.min.js"></script>
<script src="<?= base_url('src/') ?>js/owl.carousel.min.js"></script>
<script src="<?= base_url('src/') ?>js/jquery.sticky.js"></script>
<script src="<?= base_url('src/') ?>js/jquery.waypoints.min.js"></script>
<script src="<?= base_url('src/') ?>js/jquery.animateNumber.min.js"></script>
<script src="<?= base_url('src/') ?>js/jquery.fancybox.min.js"></script>
<script src="<?= base_url('src/') ?>js/jquery.easing.1.3.js"></script>
<script src="<?= base_url('src/') ?>js/aos.js"></script>
<script src="<?= base_url('src') ?>/sweetalert/dist/sweetalert2.min.js"></script>
<script id="midtrans-script" src="https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js" data-environment="<production|sandbox>" data-client-key="SB-Mid-client-Vv0L5QJGtDhWHJwh" type="text/javascript">
</script>
<script src="<?= base_url('src/') ?>js/main.js"></script>
<?php if ($uri == 'boking') : ?>
	<script src="<?= base_url('src/datetime') ?>/build/jquery.datetimepicker.full.min.js"></script>
	<script src="<?= base_url('src/myjs/boking.js') ?>"></script>
<?php endif ?>
<?php if ($uri == 'daftar_tiket') : ?>
	<script type="text/javascript" src="<?= base_url('src/datatable/assets/js/jquery.dataTables.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('src/datatable/assets/js/dataTables.bootstrap4.min.js') ?>"></script>
	<script src="<?= base_url('src/myjs/daftar_tiket.js') ?>"></script>
<?php endif ?>

<?php if ($uri == 'invoice') : ?>
	<script src="<?= base_url('src/myjs/invoice.js') ?>"></script>
<?php endif ?>

<?php if ($uri == 'home') : ?>
	<script src="<?= base_url('src/myjs/home.js') ?>"></script>
<?php endif ?>

<?php if ($uri == 'akun') : ?>
	<script src="<?= base_url('src/datetime') ?>/build/jquery.datetimepicker.full.min.js"></script>
	<script src="<?= base_url('src/myjs/akun.js') ?>"></script>
<?php endif ?>
<?php if ($uri == 'pilih-kursi') : ?>
	<script src="<?= base_url('src/myjs/pilih_kursi.js') ?>"></script>
<?php endif ?>
<?php if ($uri == 'kursi') : ?>
	<script src="<?= base_url('src/datetime') ?>/build/jquery.datetimepicker.full.min.js"></script>

	<script src="<?= base_url('src/myjs/kursi-user.js') ?>"></script>
<?php endif ?>
<script>
	$(document).ready(function() {

		let url = window.location;
		// link biasa
		$('nav .site-menu li a').filter(function() {

			if (this.href == url.href) {
				$(this).addClass('active');
			}
		});
	});
</script>
</body>

</html>