<!-- Modal -->
<div class="modal fade" id="modalKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= form_open_multipart(base_url('user/Daftar_tiket/konfirmasi_pembayaran'), 'id="formKonfirmasi"'); ?>
      <div class="modal-body">
        <input type="hidden" name="id" id="id_boking">
        <div class="form-group">
          <label for="nama">Nama Akun</label>
          <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan nama" aria-describedby="errorNama">
          <small id="errorNama" class="invalid-feedback"></small>
        </div>
        <div class="form-group">
          <label for="file">Bukti pembayaran</label>
          <input type="file" name="file" id="file" class="form-control" placeholder="" aria-describedby="errorFile">
          <small id="errorFile" class="invalid-feedback"></small>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      <?= form_close() ?>
    </div>
  </div>
</div>