<?php
$id_user = $this->session->userdata('id_user');
if ($id_user != null) {
  $tiket = $this->db->get_where('boking', ['id_user' => $id_user, 'status' => '1'])->num_rows();
}
?>
<header class="site-navbar js-sticky-header site-navbar-target" role="banner">
  <div class="container">
    <div class="row align-items-center position-relative">
      <div class="site-logo">
        <a href="home" class="text-black"><span class="text-primary">Cargo</span></a>
      </div>
      <div class="col-12">
        <nav class="site-navigation text-right ml-auto " role="navigation">

          <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
            <li><a href="<?= base_url('home') ?>" class="nav-link">Home</a></li>
            <li><a href="<?= base_url('jadwal-pelayaran') ?>" class="nav-link">Jadwal Pelayaran</a></li>
            <li><a href="<?= base_url('kursi') ?>" class="nav-link">Kursi</a></li>
            <li><a href="<?= base_url('boking') ?>" class="nav-link">Boking Tiket</a></li>
            <?php if ($id_user) : ?>
              <li><a href="<?= base_url('daftar_tiket') ?>" class="nav-link">Tiket saya <?= $tiket > 0 ? '<span class="badge badge-danger jumlah-tiket">' . $tiket . '</span>' : '' ?></a></li>
            <?php endif ?>

            <li><a href="#contact-section" class="nav-link">Contact</a></li>
          </ul>
        </nav>
      </div>
      <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

    </div>
  </div>

</header>