<?php $uri = $this->uri->segment(1); ?>
<!doctype html>

<html lang="en">

<head>
    <title>Pemesanan Tiket</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700|Oswald:400,700" rel="stylesheet"> -->

    <link rel="stylesheet" href="<?= base_url('src/') ?>fonts/icomoon/style.css">

    <link rel="stylesheet" href="<?= base_url('src/') ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('src/') ?>css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?= base_url('src/') ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url('src/') ?>css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?= base_url('src/') ?>fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="<?= base_url('src/') ?>css/aos.css">

    <link rel="stylesheet" href="<?= base_url('src') ?>/sweetalert/dist/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('src/datetime') ?>/jquery.datetimepicker.css" />
    <!-- MAIN CSS -->

    <link rel="stylesheet" href="<?= base_url('src/') ?>css/style.css">
    <link rel="stylesheet" href="<?= base_url('src/datatable/assets/css/') ?>dataTables.bootstrap4.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <?php if ($uri == 'pilih-kursi') : ?>
        <link rel="stylesheet" href="<?= base_url('src/') ?>css/mystyle.css">
    <?php endif ?>
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">