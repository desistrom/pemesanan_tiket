<div class="container">
  <div class="mt-3 mb-5">
    <div class="table-responsive">
      <table id="tblDaftarTiket" class="table table-striped" style="width:100%">
        <thead class="bg-primary text-white">
          <tr>
            <th>No faktur</th>
            <th>Kapal</th>
            <th>Rute</th>
            <th>Tanggal pelayaran</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>

    </div>

  </div>
</div>