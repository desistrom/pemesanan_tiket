<?php
function array_reindex($array)
{
  if (is_array($array)) {
    return array_map('array_reindex', array_values($array));
  } else {
    return $array;
  }
}
$coba = array(1, 2, 3, 4);
$array = $this->db->select('id_kursi')->from('kursi')->get()->result_array();
echo 'ini adalah sebelum di convert';
var_dump($array);
$test = [];
foreach ($array as $v) {
  $test[] = $v['id_kursi'];
}
echo 'ini adalah sesdah di convert';
echo '<h1>' . $test[1] . '</h1>';
var_dump($coba);
var_dump($test);
die;
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="<?= base_url('src/datetime') ?>/jquery.datetimepicker.css" />
</head>

<body>
  Masukkan tanggal
  <form action="" method="post" autocomplete="disable">
    <input id="datetimepicker" type="text">
    <button type="submit">Test</button>
  </form>

  <script src="<?= base_url('src/') ?>js/jquery-3.3.1.min.js"></script>
  <script src="<?= base_url('src/datetime') ?>/build/jquery.datetimepicker.full.min.js"></script>
  <script>
    jQuery('#datetimepicker').datetimepicker();
  </script>
</body>

</html>