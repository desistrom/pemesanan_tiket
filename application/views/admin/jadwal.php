<div class="app-main__outer">
  <div class="app-main__inner">
    <div class="app-page-title">
      <div class="page-title-wrapper">
        <div class="page-title-heading">
          <div class="page-title-icon">
            <i class="pe-7s-map-2 icon-gradient bg-mean-fruit">
            </i>
          </div>
          <div>Jadwal
            <div class="page-title-subheading">Jadwal pelayaran kapal
            </div>
          </div>
        </div>
        <div class="page-title-actions">
          <button id="btnInsert" type="button" data-toggle="tooltip" title="Tambah Rute" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
            Tambah Jadwal <i class="fa fa-plus-circle"></i>
          </button>
          <button id="btnView" style="display:none;" class="btn-shadow mr-3 btn btn-success">View data</button>
        </div>
      </div>
    </div>
    <div class="alert alert-success d-none" style="clear:both;overflow: hidden;" id="alertAction">
      <span id="alert" class="float-left"></span>
      <a href="#" id="closeAlert"><i class="fa fa-times float-right"></i></a>
    </div>
    <div id="data" class="bg-white shadow rounded p-3">
      <table id="table" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th width="10">No</th>
            <th>Kapal</th>
            <th>Tempat</th>
            <th>Tujuan</th>
            <th>Jam</th>
            <th width="30">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <div id="action" class="bg-white shadow rounded p-3" style="display: none">
      <?= form_open("", 'id="formAction"') ?>
      <input type="hidden" name="id" class="d-none" id="idJadwal">
      <div class="position-relative row form-group">
        <label for="rute" class="col-sm-2 col-form-label">Rute</label>
        <div class="col-sm-10">
          <select name="id_rute" id="idRute" class="form-control">
            <option value="">Pilih Rute</option>
            <?php foreach ($rute->result() as $value) : ?>
              <option value="<?= $value->id_rute ?>"><?= $value->tempat ?> - <?= $value->tujuan ?></option>
            <?php endforeach ?>
          </select>
          <span class="invalid-feedback" id="errorIdRute"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="idKapal" class="col-sm-2 col-form-label">Kapal</label>
        <div class="col-sm-10">
          <select name="id_kapal" id="idKapal" class="form-control">
            <option value="">Pilih Kapal</option>
            <?php foreach ($kapal->result() as $value) : ?>
              <option value="<?= $value->id_kapal ?>"><?= $value->nama ?></option>
            <?php endforeach ?>
          </select>
          <span class="invalid-feedback" id="errorIdKapal"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="hari" class="col-sm-2 col-form-label">Hari</label>
        <div class="col-sm-10">
          <select name="hari" id="hari" class="form-control">
            <option value="">Pilih hari</option>
            <option value="senin">senin</option>
            <option value="selasa">selasa</option>
            <option value="rabu">rabu</option>
            <option value="kamis">kamis</option>
            <option value="jumat">jumat</option>
            <option value="sabtu">sabtu</option>
            <option value="minggu">minggu</option>
          </select>
          <span class="invalid-feedback" id="errorHari"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="jam" class="col-sm-2 col-form-label">Jam</label>
        <div class="col-sm-10">
          <input name="jam" id="jam" placeholder="masukkan jam" type="time" class="form-control">
          <span class="invalid-feedback" id="errorJam"></span>
        </div>
      </div>
      <div class="position-relative row">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
          <button type="submit" class="btn btn-secondary">Save</button>
        </div>
      </div>
      <?= form_close() ?>
    </div>
  </div>
</div>