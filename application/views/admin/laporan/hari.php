<div class="app-main__outer">
  <div class="app-main__inner">
    <div class="bg-white shadow rounded p-3">
      <div class="card">
        <div class="card-header d-block pt-3">
          Laporan Perhari
          <div class="float-right">
            <button class="btn btn-success" id="btnPrint"><i class="fa fa-print"></i> Print</button>
          </div>
        </div>
        <div class="card-body bg-dark p-5">
          <div id="print" class="bg-white rounded p-3 text-dark">
            <div>
              <h4 class="text-center">LAPORAN PERHARI</h4>

            </div>
            <div class="mb-1">tanggal : <?= $tgl; ?>
            </div>
            <table class="table table-bordered table-sm">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">No faktur</th>
                  <th scope="col">nama</th>
                  <th scope="col">email</th>
                  <th scope="col">kursi</th>
                  <th scope="col">rute</th>
                  <th scope="col">Tanggal Boking</th>
                  <th scope="col">Tanggal Pelayaran</th>
                  <th scope="col">IDR</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($laporan->result() as $lv) : ?>
                  <tr>
                    <td scope="row"><?= $no ?></td>
                    <td><?= $lv->no_faktur ?></td>
                    <td><?= $lv->nama ?></td>
                    <td><?= $lv->email ?></td>
                    <td><?= $lv->kursi ?></td>
                    <td><?= $lv->tempat . ' - ' . $lv->tujuan ?></td>
                    <td><?= $lv->tgl_boking ?></td>
                    <td><?= $lv->tgl_pelayaran ?></td>
                    <td><?= 'Rp' . number_format($lv->total) ?></td>
                  </tr>
                <?php $no++;
                endforeach ?>
                <tr>
                  <td class="text-center" colspan="8">Total</td>
                  <td><?= 'Rp' . number_format($total->total) ?></td>
                </tr>
              </tbody>
            </table>
            <div class="mt-5" style="overflow:auto">
              <div class="text-center mt-5" style="width:390px; float:right">
                <div class="mb-5">Admin pemesanan tiket</div>
                <div class="mt-5">Bojonegoro, <?= date('d F Y') ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->