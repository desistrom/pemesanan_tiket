<?php $url = $this->uri->segment(1); ?>
<script>
  let base_url = '<?= base_url() ?>';
</script>
<script type="text/javascript" src="<?= base_url('src/admin/') ?>scripts/main.js"></script>
<script type="text/javascript" src="<?= base_url('src/js/jquery-3.3.1.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('src/admin/dateTime/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('src/datatable/assets/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('src/datatable/assets/js/dataTables.bootstrap4.min.js') ?>"></script>

<?php if ($url == 'rute') : ?>
  <script src="<?= base_url('src/myjs/rute.js') ?>"></script>
<?php endif ?>
<?php if ($url == 'kota') : ?>
  <script src="<?= base_url('src/myjs/kota.js') ?>"></script>
<?php endif ?>
<?php if ($url == 'kapal') : ?>
  <script src="<?= base_url('src/myjs/kapal.js') ?>"></script>
<?php endif ?>
<?php if ($url == 'kabin') : ?>
  <script src="<?= base_url('src/myjs/kabin.js') ?>"></script>
<?php endif ?>
<?php if ($url == 'admin-kursi') : ?>
  <script src="<?= base_url('src/myjs/kursi.js') ?>"></script>
<?php endif ?>
<?php if ($url == 'jadwal') : ?>
  <script src="<?= base_url('src/myjs/jadwal.js') ?>"></script>
<?php endif ?>
<?php if ($url == 'daftar-boking') : ?>
  <script src="<?= base_url('src/myjs/daftar_boking.js') ?>"></script>
<?php endif ?>
<?php if ($url == 'add-admin') : ?>
  <script src="<?= base_url('src/myjs/tambah_admin.js') ?>"></script>
<?php endif ?>
<?php if ($url == 'laporan') : ?>
  <script src="<?= base_url('src/datetime') ?>/build/jquery.datetimepicker.full.min.js"></script>
  <script src="<?= base_url('src/myjs/laporan.js') ?>"></script>
<?php endif ?>

<?php if ($url == 'laporan-hari') : ?>
  <script src="<?= base_url('src/printThis') ?>/printThis.js"></script>
  <script src="<?= base_url('src/myjs/laporan-print.js') ?>"></script>
<?php endif ?>

<?php if ($url == 'laporan-bulan') : ?>
  <script src="<?= base_url('src/printThis') ?>/printThis.js"></script>
  <script src="<?= base_url('src/myjs/laporan-print.js') ?>"></script>
<?php endif ?>

<script>
  $(document).ready(function() {

    let url = window.location;
    // link biasa
    $('.vertical-nav-menu li a').filter(function() {
      if (this.href == url.href) {
        $(this).addClass('mm-active');
      }
    });
  });
</script>
</body>

</html>