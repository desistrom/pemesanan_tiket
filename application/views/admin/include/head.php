<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Language" content="en">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Admin | pemesanan tiket</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
  <meta name="description" content="This is an example dashboard created using build-in elements and components.">
  <meta name="msapplication-tap-highlight" content="no">
  <style>
    @page {
      size: auto;
      margin: 0mm;
    }

    .lds-facebook {
      position: relative;
      margin-top: 200px;
      width: 80px;
      height: 80px;
      margin-left: auto;
      margin-right: auto;
    }


    .loader2 {
      background: #000000;
      opacity: 0.5;
      position: fixed;
      z-index: 999999;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
    }

    .lds-facebook div {
      display: inline-block;
      position: absolute;
      left: 8px;
      width: 16px;
      background: #fff;
      animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    }

    .lds-facebook div:nth-child(1) {
      left: 8px;
      animation-delay: -0.24s;
    }

    .lds-facebook div:nth-child(2) {
      left: 32px;
      animation-delay: -0.12s;
    }

    .lds-facebook div:nth-child(3) {
      left: 56px;
      animation-delay: 0;
    }

    @keyframes lds-facebook {
      0% {
        top: 8px;
        height: 64px;
      }

      50%,
      100% {
        top: 24px;
        height: 32px;
      }
    }
  </style>
  <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
  <link href="<?= base_url('src/admin/css/') ?>main.css" rel="stylesheet">

  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
  <link rel="stylesheet" href="<?= base_url('src/datatable/assets/css/') ?>dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url('src/datetime') ?>/jquery.datetimepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('src/') ?>admin/dateTime/css/datepicker.css" />


</head>

<body>