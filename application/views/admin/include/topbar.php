<?php

$id_user = $this->session->userdata('id_user');
$user    = $this->db->get_where('user', ['id_user' => $id_user])->row();
$uri = $this->uri->segment(1);

?>

<div id="loader2" class="loader2 d-none">
  <div class="lds-facebook">
    <div></div>
    <div></div>
    <div></div>
  </div>
</div>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header <?= $uri == 'daftar-boking' || $uri == 'laporan-hari' || $uri == 'laporan-bulan'  ? 'closed-sidebar' : '' ?> ">
  <div class="app-header header-shadow bg-asteroid header-text-light">
    <div class="app-header__logo">
      <div class="logo-src"></div>
      <div class="header__pane ml-auto">
        <div>
          <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
        </div>
      </div>
    </div>
    <div class="app-header__mobile-menu">
      <div>
        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
      </div>
    </div>
    <div class="app-header__menu">
      <span>
        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
          <span class="btn-icon-wrapper">
            <i class="fa fa-ellipsis-v fa-w-6"></i>
          </span>
        </button>
      </span>
    </div>
    <div class="app-header__content">
      <div class="app-header-right">
        <div class="header-btn-lg pr-0">
          <div class="widget-content p-0">
            <div class="widget-content-wrapper">
              <div class="widget-content-left">
                <div class="btn-group">
                  <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                    <div class="widget-content-left  ml-3 header-user-info float-left">
                      <div class="widget-heading">
                        <?= ucwords($user->nama) ?>
                      </div>
                      <div class="widget-subheading">
                        <?= $user->level == 1 ? 'Admin' : 'user' ?>
                      </div>
                    </div>
                    <div class="float-right">
                      <i class="fa fa-angle-down ml-2 opacity-8"></i>

                    </div>
                  </a>
                  <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                    <button type="button" tabindex="0" class="dropdown-item">User Account</button>
                    <button type="button" tabindex="0" class="dropdown-item">Settings</button>
                    <h6 tabindex="-1" class="dropdown-header">Header</h6>
                    <button type="button" tabindex="0" class="dropdown-item">Actions</button>
                    <div tabindex="-1" class="dropdown-divider"></div>
                    <a href="<?= base_url('logout') ?>" class="dropdown-item">Logout</a>
                  </div>
                </div>
              </div>

              <div class="widget-content-right header-user-info ml-3">
                <button type="button" class="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example">
                  <i class="fa text-white fa-calendar pr-1 pl-1"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>