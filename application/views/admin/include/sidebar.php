<div class="app-sidebar sidebar-shadow bg-royal sidebar-text-light">
  <div class="app-header__logo">
    <div class="logo-src"></div>
    <div class="header__pane ml-auto">
      <div>
        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
      </div>
    </div>
  </div>
  <div class="app-header__mobile-menu">
    <div>
      <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
    </div>
  </div>
  <div class="app-header__menu">
    <span>
      <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
        <span class="btn-icon-wrapper">
          <i class="fa fa-ellipsis-v fa-w-6"></i>
        </span>
      </button>
    </span>
  </div>
  <div class="scrollbar-sidebar">
    <div class="app-sidebar__inner">
      <ul class="vertical-nav-menu">
        <li class="app-sidebar__heading">Menu</li>

        <li>
          <a href="<?= base_url('add-admin') ?>">
            <i class="metismenu-icon fa fa-user-tie"></i>
            Tambah admin
          </a>
        </li>
        <li>
          <a href="<?= base_url('kapal') ?>">
            <i class="metismenu-icon fa fa-ship"></i>
            Kapal
          </a>
        </li>
        <li>
          <a href="<?= base_url('rute') ?>">
            <i class="metismenu-icon fa fa-map"></i>
            Rute
          </a>
        </li>
        <li>
          <a href="<?= base_url('kabin') ?>">
            <i class="metismenu-icon fa fa-stream"></i>
            Kabin
          </a>
        </li>
        <li>
          <a href="<?= base_url('admin-kursi') ?>">
            <i class="metismenu-icon fa fa-chair"></i>
            Kursi
          </a>
        </li>
        <li>
          <a href="<?= base_url('jadwal') ?>">
            <i class="metismenu-icon fa fa-calendar"></i>
            Jadwal
          </a>
        </li>
        <li>
          <a href="<?= base_url('daftar-boking') ?>">
            <i class="metismenu-icon fa fa-list"></i>
            Daftar boking
          </a>
        </li>
        <li>
          <a href="<?= base_url('laporan') ?>">
            <i class="metismenu-icon fa fa-file"></i>
            Laporan
          </a>
        </li>
        <li>
      </ul>
    </div>
  </div>
</div>