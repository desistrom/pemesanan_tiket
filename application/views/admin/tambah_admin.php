<div class="app-main__outer">
  <div class="app-main__inner">
    <div class="app-page-title">
      <div class="page-title-wrapper">
        <div class="page-title-heading">
          <div class="page-title-icon">
            <i class="fa fa-user-tie icon-gradient bg-mean-fruit">
            </i>
          </div>
          <div>Tambah admin
            <div class="page-title-subheading">untuk admin yang bisa mengakses menu admin
            </div>
          </div>
        </div>
        <div class="page-title-actions">
          <button id="btnInsert" type="button" data-toggle="tooltip" title="Tambah Rute" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
            Tambah Admin <i class="fa fa-plus-circle"></i>
          </button>
          <button id="btnView" style="display:none;" class="btn-shadow mr-3 btn btn-success">View data</button>
        </div>
      </div>
    </div>
    <div class="alert alert-success d-none" style="clear:both;overflow: hidden;" id="alertAction">
      <span id="alert" class="float-left"></span>
      <a href="#" id="closeAlert"><i class="fa fa-times float-right"></i></a>
    </div>
    <div id="data" class="bg-white shadow rounded p-3">
      <table id="table" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th width="10">No</th>
            <th>Email</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <div id="action" class="bg-white shadow rounded p-3" style="display: none">
      <?= form_open("", 'id="formAction"') ?>
      <input type="hidden" name="id" class="d-none" id="idRute">
      <div class="position-relative row form-group">
        <label for="email" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
          <input name="email" id="email" placeholder="Masukan email " type="text" class="form-control">
          <span class="invalid-feedback" id="errorEmail"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
        <div class="col-sm-10">
          <input name="nama" id="nama" placeholder="Masukan nama " type="text" class="form-control">
          <span class="invalid-feedback" id="errorNama"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
        <div class="col-sm-10">
          <textarea name="alamat" placeholder="Masukkan alamat" id="alamat" class="form-control"></textarea>
          <span class="invalid-feedback" id="errorAlamat"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="password" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
          <input name="password" id="password" placeholder="Masukkan password" type="number" class="form-control">
          <span class="invalid-feedback" id="errorPassword"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="password2" class="col-sm-2 col-form-label">Konfirmasi password</label>
        <div class="col-sm-10">
          <input name="password2" id="password2" placeholder="Masukkan Konfirmasi password" type="number" class="form-control">
          <span class="invalid-feedback" id="errorPassword2"></span>
        </div>
      </div>
      <div class="position-relative row">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
          <button type="submit" class="btn btn-secondary">Save</button>
        </div>
      </div>
      <?= form_close() ?>
    </div>
  </div>
</div>