<div class="app-main__outer">
  <div class="app-main__inner">
    <div class="app-page-title">
      <div class="page-title-wrapper">
        <div class="page-title-heading">
          <div class="page-title-icon">
            <i class="pe-7s-map-2 icon-gradient fa fa-file">
            </i>
          </div>
          <div>Laporan
            <div class="page-title-subheading">Laporan Mencakup Penjualan tiket Perhari, Perbulan, Pertahun
            </div>
          </div>
        </div>
        <div class="page-title-actions">
          <button id="btnView" style="display:none;" class="btn-shadow mr-3 btn btn-success">View data</button>
        </div>
      </div>
    </div>
    <?php if ($this->session->flashdata('msg')) : ?>
      <div class="alert alert-success" style="clear:both;overflow: hidden;" id="alertAction">
        <span id="alert" class="float-left"><?= $this->session->flashdata('msg');; ?>
        </span>
        <a href="#" id="closeAlert"><i class="fa fa-times float-right"></i></a>
      </div>
    <?php endif  ?>
    <div id="data" class="bg-white shadow rounded p-3">
      <ul class="list-group list-group-flush">
        <li class="list-group-item d-flex justify-content-between align-items-center">
          Laporan Perhari
          <button data-target="#modalLaporan" data-toggle="modal" class="btn btn-outline-primary" id="laporanHari">Print <i class="fa fa-print"></i></button>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          Laporan perbulan
          <button class="btn btn-outline-primary" id="laporanBulan">Print <i class="fa fa-print"></i></button>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal -->

<div id="action" class="bg-white shadow rounded p-3" style="display: none ; width: 25%">
  <?= form_open(base_url('user/Daftar_tiket/konfirmasi_pembayaran'), 'id="formLaporan" autocomplete="false" '); ?>
  <div class="modal-body">
    <div class="form-group">
      <label for="value" id="labelLaporan"></label>
      <input type="text" name="value" id="value" class="form-control" autocomplete="off">
      <small id="errorValue" class="invalid-feedback"></small>
    </div>
    <button class="btn btn-primary btn-sm" type="submit">Print <i class="fa fa-print"></i></button>
    <button class="btn btn-secondary btn-sm cancel" type="button">Cancel</i></button>
  </div>
  <?= form_close() ?>
</div>