<div class="app-main__outer">
  <div class="app-main__inner">
    <div class="app-page-title">
      <div class="page-title-wrapper">
        <div class="page-title-heading">
          <div class="page-title-icon">
            <i class="pe-7s-map-2 icon-gradient bg-mean-fruit">
            </i>
          </div>
          <div>Boking
            <div class="page-title-subheading">Daftar Tiket yang sudah di boking oleh user
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="data" class="bg-white shadow rounded p-3">
      <table id="table" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No faktur</th>
            <th>Nama </th>
            <th>Rute</th>
            <th>Tanggal Pelayaran</th>
            <th>Tanggal Boking</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <div id="updateStatusHeadler" class="bg-white shadow rounded p-3" style="display: none;">
      <?= form_open(base_url('admin/Boking/update_status'), 'id="updateStatusBoking"') ?>
      <input type="hidden" name="id_boking" id="idUpdateStatusBoking">
      <div class="modal-body">
        <div class="form-group">
          <label for="status">Status</label>
          <select name="status" id="bokingStatus" class="form-control col-6">
            <option value="">Pilih status</option>
            <option value="0">Transaksi batal</option>
            <option value="1">Transaksi pending</option>
            <option value="2">Transaksi berhasil</option>
          </select>
          <small id="bokingStatusError" class="invalid-feedback"></small>
        </div>
        <div class="form-group">
          <button type="button" class="btn btn-secondary" id="btnCloseForm">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>