<div class="app-main__outer">
  <div class="app-main__inner">
    <div class="app-page-title">
      <div class="page-title-wrapper">
        <div class="page-title-heading">
          <div class="page-title-icon">
            <i class="pe-7s-map-2 icon-gradient bg-mean-fruit">
            </i>
          </div>
          <div>kabin
            <div class="page-title-subheading">Di gunakan Menentukan letak kursi pada kabin
            </div>
          </div>
        </div>
        <div class="page-title-actions">
          <button id="btnInsert" type="button" data-toggle="tooltip" title="Tambah Rute" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
            Tambah Kabin <i class="fa fa-plus-circle"></i>
          </button>
          <button id="btnView" style="display:none;" class="btn-shadow mr-3 btn btn-success">View data</button>
        </div>
      </div>
    </div>
    <div class="alert alert-success d-none" style="clear:both;overflow: hidden;" id="alertAction">
      <span id="alert" class="float-left"></span>
      <a href="#" id="closeAlert"><i class="fa fa-times float-right"></i></a>
    </div>
    <div id="data" class="bg-white shadow rounded p-3">
      <table id="table" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th width="10">No</th>
            <th>Kabin</th>
            <th>Status</th>
            <th width="30">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <div id="action" class="bg-white shadow rounded p-3" style="display: none">
      <?= form_open("", 'id="formAction"') ?>
      <input type="hidden" name="id" class="d-none" id="idKabin">
      <div class="position-relative row form-group">
        <label for="kabin" class="col-sm-2 col-form-label">Kabin</label>
        <div class="col-sm-10">
          <input name="kabin" id="kabin" placeholder="Masukan kabin " type="number" class="form-control">
          <span class="invalid-feedback" id="errorKabin"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="status" class="col-sm-2 col-form-label">Status</label>
        <div class="col-sm-10">
          <select name="status" id="status" class="form-control">
            <option value="">Pilih status</option>
            <option value="on">on</option>
            <option value="off">off</optionval>
          </select>
          <span class="invalid-feedback" id="errorStatus"></span>
        </div>
      </div>
      <div class="position-relative row">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
          <button type="submit" class="btn btn-secondary">Save</button>
        </div>
      </div>
      <?= form_close() ?>
    </div>
  </div>
</div>