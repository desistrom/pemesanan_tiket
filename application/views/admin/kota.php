<div class="app-main__outer">
  <div class="app-main__inner">
    <div class="app-page-title">
      <div class="page-title-wrapper">
        <div class="page-title-heading">
          <div class="page-title-icon">
            <i class="pe-7s-map-2 icon-gradient bg-mean-fruit">
            </i>
          </div>
          <div>Kota
            <div class="page-title-subheading">Kota-kota yang di lewati pada rute
            </div>
          </div>
        </div>
        <div class="page-title-actions">
          <button id="btnInsert" type="button" data-toggle="tooltip" title="Tambah Rute" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary">
            Tambah Kota <i class="fa fa-plus-circle"></i>
          </button>
          <button id="btnView" style="display:none;" class="btn-shadow mr-3 btn btn-success">View data</button>
        </div>
      </div>
    </div>
    <div class="alert alert-success d-none" style="clear:both;overflow: hidden;" id="alertAction">
      <span id="alert" class="float-left"></span>
      <a href="#" id="closeAlert"><i class="fa fa-times float-right"></i></a>
    </div>
    <div id="data" class="bg-white shadow rounded p-3">
      <table id="table" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th width="10">No</th>
            <th>Rute</th>
            <th>Kota</th>
            <th>Jarak</th>
            <th>Posisi</th>
            <th width="30">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <div id="action" class="bg-white shadow rounded p-3" style="display: none">
      <?= form_open("", 'id="formAction"') ?>
      <input type="hidden" name="id" class="d-none" id="idKota">
      <div class="position-relative row form-group">
        <label for="tarif" class="col-sm-2 col-form-label">Rute</label>
        <div class="col-sm-10">
          <select name="id_rute" id="idRute" class="form-control">
            <option value="">Pilih Rute</option>
            <?php $no = 1;
            foreach ($rute->result() as $value) : ?>
              <option value="<?= $value->id_rute ?>"><?= $value->rute ?></option>
            <?php $no++;
            endforeach ?>
          </select>
          <span class="invalid-feedback" id="errorIdRute"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="kota" class="col-sm-2 col-form-label">Kota</label>
        <div class="col-sm-10">
          <input name="kota" id="kota" placeholder="Masukan kota " type="text" class="form-control">
          <span class="invalid-feedback" id="errorKota"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="tarif" class="col-sm-2 col-form-label">Jarak</label>
        <div class="col-sm-10">
          <input name="jarak" id="jarak" placeholder="masukkan jarak" type="text" class="form-control">
          <span class="invalid-feedback" id="errorJarak"></span>
        </div>
      </div>
      <div class="position-relative row form-group">
        <label for="tarif" class="col-sm-2 col-form-label">Posisi</label>
        <div class="col-sm-10">
          <input name="posisi" id="posisi" placeholder="masukkan posisi" type="number" class="form-control">
          <span class="invalid-feedback" id="errorPosisi"></span>
        </div>
      </div>
      <div class="position-relative row">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
          <button type="submit" class="btn btn-secondary">Save</button>
        </div>
      </div>
      <?= form_close() ?>
    </div>
  </div>
</div>