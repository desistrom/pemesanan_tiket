<div class="container">
  <div class="row">
    <div class="col-sm-12 mt-3 col-lg-6 mx-auto">
      <div class="div">
        <div class="container py-3 ftco-cover-1 overlay rounded shadow">
          <div class="row align-items-center">
            <div class="col-lg-12 ">
              <h1 class="mb-3" style="border-bottom: 4px solid #fff">Registrasi</h1>
              <?= form_open(base_url('Register/registrasi'), 'id="formRegister"') ?>
              <div class="form-group">
                <label for="nama" class="text-white">Nama</label>
                <input autofocus type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan nama">
                <small id="errorNama" class="text-danger"></small>
              </div>
              <div class="form-group">
                <label for="email" class="text-white">Email</label>
                <input type="text" name="email" id="email" class="form-control" placeholder="Masukkan email">
                <small id="errorEmail" class="text-danger"></small>
              </div>
              <div class="form-group">
                <label for="alamat" class="text-white">alamat</label>
                <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Masukkan alamat">
                <small id="errorAlamat" class="text-danger"></small>
              </div>
              <div class="form-group">
                <label for="password" class="text-white">Password</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="Masukkan password">
                <small id="errorPassword" class="text-danger"></small>
              </div>
              <div class="form-group">
                <label for="password2" class="text-white">Ulang password</label>
                <input type="password" name="password2" id="password2" class="form-control" placeholder="Masukkan ulang password">
                <small id="errorPassword2" class="text-danger"></small>
              </div>
              <button type="submit" class="btn btn-primary btn-lg col-sm-12"><span class="text-white">Register</span></button>
              <?= form_close() ?>
              <span class="border-bottom border-white d-block mt-3 mb-1"></span>
              <p class="text-center">Sudah memiliki akun? <a href="<?= base_url('Login') ?>">Login </a> <br> kembali ke home <a href="<?= base_url('home') ?>">Home</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>