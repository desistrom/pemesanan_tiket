<?php

$url = '';
$boking = $this->input->get('boking');

$boking == true ? $url = base_url('Login/login?boking=true') : $url = base_url('Login/login');

?>
<div class="container">
  <div class="row">
    <div class="col-sm-12 mt-5 col-lg-6 mx-auto">
      <div class="div">
        <div class="container py-3 ftco-cover-1 overlay rounded shadow">
          <div class="row align-items-center">
            <div class="col-lg-12 ">
              <h1 class="mb-3" style="border-bottom: 4px solid #fff">Login</h1>
              <div id="alert"><?= $this->session->flashdata('msg'); ?></div>
              <?= form_open($url, 'id="formLogin"') ?>
              <div class="form-group">
                <label for="email" class="text-white">Email</label>
                <input type="text" autofocus name="email" id="email" class="form-control" placeholder="Masukkan email">
                <small id="errorEmail" class="text-danger"></small>
              </div>
              <div class="form-group">
                <label for="password" class="text-white">Password</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="Masukkan password">
                <small id="errorPassword" class="text-danger"></small>
              </div>
              <button type="submit" class="btn btn-primary btn-lg col-sm-12"><span class="text-white">Login</span></button>
              <?= form_close() ?>
              <span class="border-bottom border-white d-block mt-3 mb-1"></span>
              <p class="text-center">Belum memiliki akun? <a href="<?= base_url('Register') ?>"> Register </a> <br> kembali ke home <a href="<?= base_url('home') ?>">Home</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>