<!doctype html>
<html lang="en">

<head>
  <title>Pemesanan Tiket</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700|Oswald:400,700" rel="stylesheet">

  <link rel="stylesheet" href="<?= base_url('src/') ?>fonts/icomoon/style.css">

  <link rel="stylesheet" href="<?= base_url('src/') ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url('src/') ?>css/jquery.fancybox.min.css">
  <link rel="stylesheet" href="<?= base_url('src/') ?>css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?= base_url('src/') ?>css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?= base_url('src/') ?>fonts/flaticon/font/flaticon.css">
  <link rel="stylesheet" href="<?= base_url('src/') ?>css/aos.css">

  <!-- MAIN CSS -->
  <link rel="stylesheet" href="<?= base_url('src/') ?>css/style.css">

</head>

<body style="background:#333" data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  <div id="loader2" class="loader2 d-none">
    <div class="lds-facebook">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
  <?= $contents ?>
  <?php $uri = strtolower($this->uri->segment(1)) ?>
  <script>
    let base_url = '<?= base_url(); ?>';
  </script>
  <script src="<?= base_url('src/') ?>js/jquery-3.3.1.min.js"></script>
  <script src="<?= base_url('src/') ?>js/popper.min.js"></script>
  <script src="<?= base_url('src/') ?>js/bootstrap.min.js"></script>
  <script src="<?= base_url('src/') ?>js/owl.carousel.min.js"></script>
  <script src="<?= base_url('src/') ?>js/jquery.sticky.js"></script>
  <script src="<?= base_url('src/') ?>js/jquery.waypoints.min.js"></script>
  <script src="<?= base_url('src/') ?>js/jquery.animateNumber.min.js"></script>
  <script src="<?= base_url('src/') ?>js/jquery.fancybox.min.js"></script>
  <script src="<?= base_url('src/') ?>js/jquery.easing.1.3.js"></script>
  <script src="<?= base_url('src/') ?>js/aos.js"></script>

  <?php if ($uri == 'register') : ?>
    <script src="<?= base_url('src/myjs/register.js') ?>"></script>
  <?php endif ?>
  <?php if ($uri == 'login') : ?>
    <script src="<?= base_url('src/myjs/login.js') ?>"></script>
  <?php endif ?>
  <script src="<?= base_url('src/') ?>js/main.js"></script>
</body>

</html>