<?php

class M_daftar_tiket extends CI_Model
{
  var $table = 'boking'; //nama tabel dari database
  var $column_order = array(null, 'rute', 'no_faktur', 'tgl_pelayaran', 'tgl_boking', 'status'); //field yang ada di table user
  var $column_search = array('rute.rute', 'no_faktur', 'status'); //field yang diizin untuk pencarian 
  var $order = array('no_faktur' => 'desc'); // default order 

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function _get_datatables_query($id_user)
  {

    $this->db->select("boking.id_boking as id_boking, id_user, boking.id_rute AS id_rute, no_faktur, DATE_FORMAT(tgl_pelayaran,'%H:%i %d %M %Y') AS tgl_pelayaran,
                      DATE_FORMAT(tgl_boking,'%H:%i %d %M %Y') AS tgl_boking,boking.status AS status, tempat, tujuan,kapal.nama as nama_kapal
              ")
      ->from($this->table)
      ->join('rute', 'boking.id_rute = rute.id_rute')
      ->join('kapal', 'boking.id_kapal = kapal.id_kapal')
      ->where('id_user', $id_user);

    $i = 0;

    foreach ($this->column_search as $item) // loop column 
    {
      if ($_POST['search']['value']) // if datatable send POST for search
      {

        if ($i === 0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if (count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if (isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if (isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables($id_user)
  {
    $this->_get_datatables_query($id_user);
    if ($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered($id_user)
  {
    $this->_get_datatables_query($id_user);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  function get_status_result($no_faktur)
  {
    // persiapkan curl
    $ch = curl_init();


    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Accept: application/json',
      'Authorization: Basic ' . base64_encode('SB-Mid-server-qszEJKz5z6C27eesF3UJWV7I') . ':'
    ));
    // set url 
    curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.midtrans.com/v2/" . $no_faktur . "/status");

    // return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output contains the output string 
    $output = curl_exec($ch);

    // tutup curl 
    curl_close($ch);

    // menampilkan hasil curl
    return $output;
  }
}
