<?php

class M_akun extends CI_Model
{
  function rules_update()
  {
    $this->form_validation->set_rules('id', 'id', 'required|numeric');
    $this->form_validation->set_rules('nama', 'Nama', 'required|max_length[50]');
    $this->form_validation->set_rules('alamat', 'Alamat', 'required');
  }

  function get_password($id)
  {
    return $this->db->select('password')
      ->from('user')
      ->where(['id_user' => $id])->get()->row();
  }
}
