<?php

class M_invoice extends CI_Model
{
  function get_boking($no_faktur)
  {
    return $this->db->select(
      "boking.id_boking as id_boking, kursi.id_kursi, boking.id_rute AS id_rute, no_faktur,DATE_FORMAT(tgl_boking,'%H:%i %d %M %Y') AS tgl_boking,DATE_FORMAT(tgl_pelayaran,'%H:%i %d %M %Y') AS tgl_pelayaran,
      boking.status AS status, tempat, tujuan, jarak, total, kursi, user.nama as nama, email, alamat,kabin, kapal.nama as nama_kapal, tarif_dewasa, tarif_anak, usia,nama_penerima,phone"
    )->from('boking')
      ->join('rute', 'boking.id_rute = rute.id_rute')
      ->join('detail_boking', 'detail_boking.id_boking = boking.id_boking')
      ->join('kursi', 'detail_boking.id_kursi = kursi.id_kursi')
      ->join('kapal', 'kursi.id_kapal = kapal.id_kapal')
      ->join('kabin', 'kursi.id_kabin = kabin.id_kabin')
      ->join('user', 'boking.id_user = user.id_user')
      ->where([
        'no_faktur' => $no_faktur,
      ])->get();
  }

  function get_detail_order($no_faktur)
  {
    return $this->db->select('total, nama, alamat, email, boking.id_rute as id_rute, tarif_anak, tarif_dewasa, tempat, tujuan')
      ->from('boking')
      ->join('user', 'boking.id_user = user.id_user')
      ->join('detail_order', 'detail_boking.id_boking = boking.id_bokig')
      ->join('rute', 'boking.id_rute = rute.id_rute')
      ->where(['no_faktur' => $no_faktur])->get();
  }
}
