<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_boking extends CI_Model
{
  function rules_boking_tiket()
  {
    $this->form_validation->set_rules('id_rute', 'Id rute', 'required');
    $this->form_validation->set_rules('id_kapal', 'Kapal', 'required');
    $this->form_validation->set_rules('tgl', 'Tanggal', 'required');
    $this->form_validation->set_rules('usia[]', 'Usia', 'required');
    $this->form_validation->set_rules('id_kursi[]', 'id_kursi', 'required');
    $this->form_validation->set_rules('nama[]', 'Nama penerima', 'required|max_length[50]');
    $this->form_validation->set_rules('phone[]', 'Phone', 'required|max_length[20]');
  }

  function get_jadwal($hari, $id_rute)
  {
    return $this->db->select('nama, jadwal.id_kapal as id_kapal, jam')
      ->from('jadwal')
      ->join('kapal', 'kapal.id_kapal = jadwal.id_kapal')
      ->where([
        'id_rute' => $id_rute,
        'hari'    => $hari
      ])->get();
  }
  // mengambil kursi berdasarkan kursi ynag sudah di  boking
  function get_kursi($id_kursi, $id_kabin, $id_kapal, $banyak_kursi, $no_kursi_pertama = null, $kursi_lebih_dari_satu = null)
  {
    $where = '';

    // jika kursi pertama ada
    if ($kursi_lebih_dari_satu) {
      $where = [
        'kursi.status' => 'on',
        'id_kabin' => $id_kabin,
        'id_kapal' => $id_kapal,
        'kursi >' => $no_kursi_pertama
      ];
    } else if ($no_kursi_pertama) {
      $where = [
        'kursi.status' => 'on',
        'id_kabin' => $id_kabin,
        'id_kapal' => $id_kapal,
        'kursi >=' => $no_kursi_pertama
      ];
    } else {
      $where = [
        'kursi.status' => 'on',
        'id_kabin' => $id_kabin,
        'id_kapal' => $id_kapal
      ];
    }
    return $this->db->select('kursi, id_kursi')
      ->from('kursi')
      ->where($where)
      ->where_not_in('id_kursi', $id_kursi)
      ->order_by('kursi', 'ASC')
      ->limit($banyak_kursi)->get();
  }

  // function kursi sudah di boking
  function get_kursi_boking($id_rute, $tgl)
  {
    return $this->db->select("kursi.id_kursi as id_kursi, kursi, DATE_FORMAT(tgl_pelayaran,'%d-%m-%Y') as tgl, boking.status as status, id_rute")
      ->from('boking')
      ->join('detail_boking', 'boking.id_boking = detail_boking.id_boking')
      ->join('kursi', 'detail_boking.id_kursi = kursi.id_kursi')
      ->where([
        'id_rute' => $id_rute,
        "DATE_FORMAT(tgl_pelayaran,'%d-%m-%Y') =" => $tgl,
      ])
      ->order_by('id_kursi', 'ASC')
      ->get();
  }

  function get_kursi_otomatis($id_rute, $tgl, $id_kapal, $kabin, $banyak_kursi)
  {
    $data_no_urut   = $this->_get_urut_boking($tgl, $id_kapal, $id_rute);
    $no_urut        = 1;
    foreach ($data_no_urut->result() as $vn) {
      if ($vn->status == '1' || $vn->status == '2') {
        $no_urut += 1;
      }
    }
    $kursi_terpakai   = $this->get_kursi_boking($id_rute, $tgl);
    $no_kursi_pertama = $kabin . '00';

    $id_kursi = null;
    // array push
    foreach ($kursi_terpakai->result() as $v) {
      if ($v->status == '1' || $v->status == '2') {
        $id_kursi[] = (int) $v->id_kursi;
      }
    }

    $kursi =  $this->get_kursi($id_kursi, $kabin, $id_kapal, $banyak_kursi, $no_kursi_pertama);
    $no_kursi_pertama = $kursi->row()->kursi;

    if ($no_urut > 1) {
      if ($banyak_kursi < 2) {
        $no_kursi_pertama = $no_kursi_pertama;
      } else {
        $kursi =  $this->get_kursi($id_kursi, $kabin, $id_kapal, $banyak_kursi, $no_kursi_pertama, true);

        $no_kursi_pertama = $kursi->row()->kursi;
      }
    }
    $kursi =  $this->get_kursi($id_kursi, $kabin, $id_kapal, $banyak_kursi, $no_kursi_pertama);

    $data = [
      'kursi_terpilih' => $kursi->result(),
      'kursi_terpakai' => $id_kursi
    ];
    return $data;
  }

  function get_tarif_dan_jam($id_rute)
  {
    return $this->db->select('tarif_dewasa, tarif_anak, jam')
      ->from('jadwal')
      ->where([
        'jadwal.id_rute' => $id_rute
      ])
      ->join('rute', 'jadwal.id_rute = rute.id_rute')->get();
  }

  function get_nofak()
  {
    $query = $this->db->query("SELECT MAX(RIGHT(no_faktur,6)) AS no_faktur FROM boking WHERE DATE(tgl_boking)=CURDATE()");
    $kd = "";
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $k) {
        $tmp = ((int) $k->no_faktur) + 1;
        $kd = sprintf("%06s", $tmp);
      }
    } else {
      $kd = "000001";
    }
    return date('dmy') . $kd;
  }

  function _get_urut_boking($tgl, $id_kapal, $id_rute)
  {
    return $this->db->select('id_boking,status')
      ->from('boking')
      ->where([
        "DATE_FORMAT(tgl_pelayaran,'%d-%m-%Y') =" => $tgl,
        'id_kapal' => $id_kapal,
        'id_rute'  =>  $id_rute,
      ])->get();
  }
}
