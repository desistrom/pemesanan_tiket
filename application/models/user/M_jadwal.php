<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_jadwal extends CI_Model
{

  public function get_hari()
  {
    return $this->db->distinct()
      ->select('hari')
      ->from('jadwal')->get();
  }

  public function get_jadwal($hari)
  {
    return $this->db->select('jadwal.id_jadwal as id_jadwal, tempat, tujuan, jarak, hari, jam,nama')
      ->from('jadwal')
      ->join('rute', 'jadwal.id_rute = rute.id_rute')
      ->join('kapal', 'jadwal.id_kapal = kapal.id_kapal')
      ->where(['hari' => $hari])
      ->order_by('jam', 'DESC')->get();
  }
}
