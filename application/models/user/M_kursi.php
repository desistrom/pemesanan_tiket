<?php

class M_kursi extends CI_Model
{
  function get_kursi($id_kabin, $id_kapal)
  {
    return $this->db->select('nama, kursi.id_kapal as id_kapal, kursi, id_kabin, id_kursi')
      ->from('kursi')
      ->join('kapal', 'kursi.id_kapal = kapal.id_kapal')
      ->where([
        'kursi.id_kapal' => $id_kapal,
        'id_kabin' => $id_kabin,
        'kursi.status'   => 'on'
      ])->get();
  }

  function get_boking_status($tgl, $id_kursi)
  {
    return $this->db->select('boking.status as status')
      ->from('boking')
      ->join('detail_boking', 'boking.id_boking = detail_boking.id_boking')
      ->where([
        "DATE_FORMAT(tgl_pelayaran,'%d-%m-%Y') = " => $tgl,
        'id_kursi' => $id_kursi
      ])->get();
  }

  function get_status($status)
  {
    if ($status) {
      // belum melakukan pembayaran
      if ($status == '1') {
        return 'bg-warning';
        // sudah terboking boking
      } else if ($status == '2') {
        return 'bg-danger';
        // belum terboking
      } else {
        return 'bg-success';
      }
    } else {
      return 'bg-success';
    }
  }

  function rules_cek_kursi()
  {
    $valid = $this->form_validation;

    $valid->set_rules('tgl', 'Tanggal', 'required');
    $valid->set_rules('id_kabin', 'Kabin', 'required|numeric');
    $valid->set_rules('id_rute', 'Rute', 'required|numeric');
    $valid->set_rules('id_kapal', 'Kapal', 'required|numeric');
  }
}
