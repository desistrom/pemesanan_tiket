<?php


class M_boking extends CI_Model
{
  var $table = 'boking'; //nama tabel dari database
  var $column_order = array(null, 'id_user', 'id_rute', 'detail_boking.id_kursi', 'no_faktur', 'tgl_pelayaran', 'tgl_boking', 'status'); //field yang ada di table user
  var $column_search = array('user.nama', 'rute.rute', 'kursi.kursi', 'no_faktur', "DATE_FORMAT(tgl_pelayaran,'%H:%I %d %M %Y')", "DATE_FORMAT(tgl_boking,'%H:%I %d %M %Y')"); //field yang diizin untuk pencarian 
  var $order = array('no_faktur' => 'desc'); // default order 

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function _get_datatables_query()
  {

    $this->db->select("boking.id_boking, boking.id_user as id_user, user.nama as nama, boking.id_rute as id_rute, tempat, tujuan, no_faktur, 
                      DATE_FORMAT(tgl_pelayaran,'%H:%I %d %M %Y') as tgl_pelayaran, DATE_FORMAT(tgl_boking,'%H:%I %d %M %Y') as tgl_boking,
                      boking.status as status")
      ->from($this->table)
      ->join('user', 'boking.id_user = user.id_user')
      ->join('rute', 'boking.id_rute = rute.id_rute');

    $i = 0;

    foreach ($this->column_search as $item) // loop column 
    {
      if ($_POST['search']['value']) // if datatable send POST for search
      {

        if ($i === 0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if (count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if (isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if (isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if ($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
}
