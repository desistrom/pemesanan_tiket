<?php

class M_laporan extends CI_Model
{
  function get_perhari($tgl)
  {
    return $this->db->select(
      "boking.id_boking, boking.id_user, kursi.id_kursi, nama,email,tempat, tujuan, kursi, no_faktur, total, 
        DATE_FORMAT(tgl_pelayaran, ' %k:%i, %d %M %Y' ) AS tgl_pelayaran, DATE_FORMAT(tgl_boking,'%d %M %Y') AS tgl_boking"
    )->from('boking')
      ->join('detail_boking', 'boking.id_boking = detail_boking.id_boking')
      ->join('rute', 'boking.id_rute = rute.id_rute')
      ->join('kursi', 'detail_boking.id_kursi = kursi.id_kursi')
      ->join('user', 'boking.id_user = user.id_user')
      ->where([
        "DATE_FORMAT(tgl_boking,'%d %M %Y') = " => $tgl,
        "boking.status" => '2',
      ])->get();
  }

  function get_total_hari($tgl)
  {
    return $this->db->select_sum('total')
      ->from('boking')
      ->where([
        "DATE_FORMAT(tgl_boking,'%d %M %Y') = " => $tgl,
        "boking.status" => '2',
      ])->get();
  }

  function get_bulan($bulan)
  {
    return $this->db->select(
      "boking.id_boking, boking.id_user, kursi.id_kursi, nama,email,tempat, tujuan, kursi, no_faktur, total, 
        DATE_FORMAT(tgl_pelayaran, ' %k:%i, %d %M %Y' ) AS tgl_pelayaran, DATE_FORMAT(tgl_boking,'%d %M %Y') AS tgl_boking"
    )->from('boking')
      ->join('detail_boking', 'boking.id_boking = detail_boking.id_boking')
      ->join('rute', 'boking.id_rute = rute.id_rute')
      ->join('kursi', 'detail_boking.id_kursi = kursi.id_kursi')
      ->join('user', 'boking.id_user = user.id_user')
      ->where([
        "DATE_FORMAT(tgl_boking,'%m-%Y') = " => $bulan,
        "boking.status" => '2',
      ])->get();
  }

  function get_total_bulan($bulan)
  {
    return $this->db->select_sum('total')
      ->from('boking')
      ->where([
        "DATE_FORMAT(tgl_boking,'%m-%Y') = " => $bulan,
        "boking.status" => '2',
      ])->get();
  }
}
