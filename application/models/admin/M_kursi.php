<?php


class M_kursi extends CI_Model
{
  var $table = 'kursi'; //nama tabel dari database
  var $column_order = array(null, 'id_kursi', 'nama', 'kursi', 'status'); //field yang ada di table user
  var $column_search = array('kursi', 'kapal.status', 'kapal.nama'); //field yang diizin untuk pencarian 
  var $order = array('id_kursi' => 'desc'); // default order 

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function _get_datatables_query()
  {

    $this->db->select('kapal.nama as nama_kapal, kursi,kabin, id_kursi,kursi.status as status')
      ->join('kapal', 'kursi.id_kapal = kapal.id_kapal')
      ->join('kabin', 'kursi.id_kabin = kabin.id_kabin')
      ->from($this->table)
      ->where([
        'kapal.status' => 'on'
      ]);

    $i = 0;

    foreach ($this->column_search as $item) // loop column 
    {
      if ($_POST['search']['value']) // if datatable send POST for search
      {

        if ($i === 0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if (count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if (isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if (isset($this->order)) {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if ($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  function rules_insert()
  {
    $this->form_validation->set_rules('id_kabin', 'Kabin', 'required|numeric');
    $this->form_validation->set_rules('id_kapal', 'Kapal', 'required|numeric');
    $this->form_validation->set_rules('kursi', 'Kursi', 'required|numeric');
    $this->form_validation->set_rules('status', 'Status', 'required');
  }

  function rules_update()
  {
    $this->form_validation->set_rules('id', 'id', 'required|numeric');
    $this->form_validation->set_rules('id_kapal', 'Kapal', 'required|numeric');
    $this->form_validation->set_rules('id_kabin', 'Kabin', 'required|numeric');
    $this->form_validation->set_rules('kursi', 'Kursi', 'required|numeric');
    $this->form_validation->set_rules('status', 'Status', 'required');
  }
}
