<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_register extends CI_Model
{
  function get_user($email)
  {
    $where = ['email' => $email];

    return $this->db->get_where('user', $where)->row();
  }

  function rules_register()
  {

    $this->form_validation->set_rules('nama', 'Nama', 'required|max_length[30]');
    $this->form_validation->set_rules('alamat', 'Alamat', 'required|max_length[120]');
    $this->form_validation->set_rules('password', 'Password', 'required|matches[password2]|min_length[6]');
    $this->form_validation->set_rules('password2', 'Ulang password', 'required|matches[password]|min_length[6]');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
  }
}
