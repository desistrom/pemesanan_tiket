<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{
  function get_user($email)
  {
    $where = ['email' => $email];

    return $this->db->get_where('user', $where)->row();
  }
}
