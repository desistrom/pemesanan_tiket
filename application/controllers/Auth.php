<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function index()
    {
        $user_level = $this->session->userdata('level');
        if ($user_level == 1) {
            redirect(base_url('daftar-boking'));
        } else {
            redirect(base_url('home'));
        }
    }

    function logout()
    {
        session_destroy();
        redirect(base_url('home'));
    }
}
