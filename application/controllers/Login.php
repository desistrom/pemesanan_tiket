<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');
    $this->login_verify->cek_login($id_user, $level);

    $this->load->model('auth/M_login', 'login');
  }

  function index()
  {
    $this->template->load('auth/template', 'auth/login');
  }

  function login()
  {
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

    if ($this->form_validation->run()) {
      $email    = $this->input->post('email', true);
      $password = $this->input->post('password', true);

      $this->_proses_login($email, $password);
    } else {
      $response = [
        'success'   => false,
        'email'     => form_error('email', '<p class="text-warning">', '</p>'),
        'password'  => form_error('password', '<p class="text-warning">', '</p>')
      ];

      echo json_encode($response);
    }
  }
  private function _proses_login($email, $password)
  {
    $user = $this->login->get_user($email);

    if ($user) {

      $password = password_verify($password, $user->password);

      if ($password) {
        $user_data = [
          'id_user' => $user->id_user,
          'level'   => $user->level
        ];

        $this->session->set_userdata($user_data);

        $response = [
          'success' => true,
        ];

        echo json_encode($response);
      } else {
        $response = [
          'success' => false,
          'password' => '<p class="text-warning">Password salah</p>'
        ];

        echo json_encode($response);
      }
    } else {
      $response = [
        'success' => false,
        'email'   => '<p class="text-warning">Email tidak terdaftar</p>'
      ];
      echo json_encode($response);
    }
  }
}
