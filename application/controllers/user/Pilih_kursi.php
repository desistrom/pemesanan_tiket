<?php
class Pilih_kursi extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('user/M_kursi', 'kursi');
    $this->load->model('user/M_boking', 'boking');
  }

  function index()
  {
    $valid = $this->form_validation;
    $this->kursi->rules_cek_kursi();

    if ($valid->run()) {
      $id_user = $this->session->userdata('id_user');


      $data['model']    = $this->kursi;
      $data['tgl']      = $this->input->post('tgl');
      $data['user']     = $this->db->get_where('user', ['id_user' => $id_user])->row();
      $data['id_rute']  = $this->input->post('id_rute');
      $data['id_kabin'] = $this->input->post('id_kabin');
      $data['id_kapal'] = $this->input->post('id_kapal');
      $data['kursi_1']  = $this->kursi->get_kursi($data['id_kabin'], $data['id_kapal']);
      $banyak_kursi     = $this->input->post('banyak_kursi');

      $array_tgl = explode(" ", $data['tgl']);

      // mengambil kursi otomatis
      $kursi_otomatis         = $this->boking->get_kursi_otomatis($data['id_rute'], $array_tgl['0'], $data['id_kapal'], $data['id_kabin'], $banyak_kursi);
      $data['kursi_otomatis'] = $kursi_otomatis['kursi_terpilih'];
      $kursi_terpakai         = $kursi_otomatis['kursi_terpakai'];
      $sisa_kursi             = $this->boking->get_kursi($kursi_terpakai, $data['id_kabin'], $data['id_kapal'], 100)->num_rows();

      if ($banyak_kursi > $sisa_kursi) {
        $this->session->set_flashdata('msg', 'kurang');
        redirect(base_url('boking'));
      } else if ($banyak_kursi == $sisa_kursi) {
        $this->session->set_flashdata('msg', 'penuh');
        redirect(base_url('boking'));
      } else {
        // tanggal
        $data['date_only']      = $array_tgl['0'];
        $this->template->load('user/template', 'user/pilih_kursi', $data);
      }
    } else {
      redirect(base_url('boking'));
    }
  }

  function cek_tarif()
  {
    $valid = $this->form_validation;
    $valid->set_rules('usia', 'Usia', 'required');
    $valid->set_rules('id_rute', 'Rute', 'required|numeric');

    if ($valid->run()) {
      $id_rute = $this->input->post('id_rute', true);
      $usia = $this->input->post('usia', true);

      $rute = $this->db->get_where('rute', ['id_rute' => $id_rute])->row();

      $response = [
        'success' => true,
        'tarif'   => $usia == 'dewasa' ? $rute->tarif_dewasa : $rute->tarif_anak,
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'rute'    => form_error('id_rute'),
        'usia'    => form_error('usia')
      ];

      echo json_encode($response);
    }
  }
}
