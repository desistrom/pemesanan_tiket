<?php

class Invoice extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    // gateway
    $params = array('server_key' => 'SB-Mid-server-qszEJKz5z6C27eesF3UJWV7I', 'production' => false);
    $this->load->library('veritrans');
    $this->veritrans->config($params);
    // model
    $this->load->model('user/M_invoice', 'invoice');
    $this->load->library('Ciqrcode');
    $this->load->library('Zend');
  }

  function index($no_faktur)
  {
    $status           = $this->get_status($no_faktur);
    $response         = json_decode($status, true);
    $data['response'] = $response;

    $data['invoice'] = $this->invoice->get_boking($no_faktur);
    if ($data['invoice']->num_rows() > 0) {

      $this->template->load('user/template', 'user/invoice', $data);
    } else {
      redirect(base_url());
    }
  }


  function cek_faktur()
  {
    $this->form_validation->set_rules('no_faktur', 'No faktur', 'required');
    if ($this->form_validation->run()) {
      $no_faktur = $this->input->post('no_faktur');

      $boking = $this->db->get_where('boking', ['no_faktur' => $no_faktur])->num_rows();

      $response = [
        'success' => true,
        'data'    => $boking
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'no_faktur'      => form_error('no_faktur', '<span class="text-warning">', '</span>')
      ];

      echo json_encode($response);
    }
  }

  function batal_transaksi($no_faktur)
  {

    $postRequest = [
      'text' => 'oke',
    ];
    $cURLConnection = curl_init("https://api.sandbox.midtrans.com/v2/" . $no_faktur . "/cancel");

    curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Accept: application/json',
      'Authorization: Basic ' . base64_encode('SB-Mid-server-qszEJKz5z6C27eesF3UJWV7I') . ':'
    ));
    curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

    $apiResponse = curl_exec($cURLConnection);
    curl_close($cURLConnection);

    // update datase
    $this->db->set('status', '0')
      ->where('no_faktur', $no_faktur)
      ->update('boking');

    redirect(base_url('invoice/') . $no_faktur);
  }

  function get_status($no_faktur)
  {


    // persiapkan curl
    $ch = curl_init();


    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Accept: application/json',
      'Authorization: Basic ' . base64_encode('SB-Mid-server-qszEJKz5z6C27eesF3UJWV7I') . ':'
    ));
    // set url 
    curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.midtrans.com//v2/" . $no_faktur . "/status");

    // return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output contains the output string 
    $output = curl_exec($ch);

    // tutup curl 
    curl_close($ch);

    // menampilkan hasil curl
    return $output;
  }

  function payment($no_faktur)
  {

    $detail_order = $this->invoice->get_boking($no_faktur);

    if ($detail_order->num_rows() > 0) {
      $order = $detail_order->row();
      $orders = $detail_order->result();
      $detail_transaksi = [
        'order_id'     => $no_faktur,
        'gross_amount' => $order->total,
      ];

      $items = [];
      foreach ($orders as $vo) {
        $items[] =  [
          'id'       => $vo->id_kursi,
          'price'    => $vo->usia == 'dewasa' ? $vo->tarif_dewasa : $vo->tarif_anak,
          'quantity' => 1,
          'name'     => $vo->kursi
        ];
      }

      $billing_address = array(
        'first_name'     => $order->nama,
        'last_name'     => null,
        'address'       => $order->alamat,
      );

      $detail_user = array(
        'first_name'       => $order->nama,
        'last_name'       => null,
        'email'           => $order->email,
        'address'         => $order->alamat,
        'billing_address' => $billing_address,
      );

      $data_transaksi = array(
        'payment_type'      => 'vtweb',
        'vtweb'             => array(
          //'enabled_payments' 	=> ['credit_card'],
          'credit_card_3d_secure' => true
        ),
        'transaction_details' => $detail_transaksi,
        'item_details'        => $items,
        'customer_details'    => $detail_user
      );

      try {
        $vtweb_url = $this->veritrans->vtweb_charge($data_transaksi);
        header('Location: ' . $vtweb_url);
      } catch (Exception $e) {
        echo $e->getMessage();
      }
    } else {
      redirect(base_url());
    }
  }

  function barcode($code)
  {
    $this->zend->load('Zend/Barcode');
    Zend_Barcode::render('Code128', 'image', array('text' => $code));
  }

  public function notification()
  {
    echo 'test notification handler';
    $json_result = file_get_contents('php://input');
    $result = json_decode($json_result);

    if ($result) {
      $notif = $this->veritrans->status($result->order_id);
    }

    error_log(print_r($result, TRUE));

    //notification handler sample

    $transaction = $notif->transaction_status;
    $type = $notif->payment_type;
    $order_id = $notif->order_id;
    $fraud = $notif->fraud_status;

    if ($transaction == 'capture') {
      // For credit card transaction, we need to check whether transaction is challenge by FDS or not
      if ($type == 'credit_card') {
        if ($fraud == 'challenge') {
          // TODO set payment status in merchant's database to 'Challenge by FDS'
          // TODO merchant should decide whether this transaction is authorized or not in MAP
          echo "Transaction order_id: " . $order_id . " is challenged by FDS";
        } else {
          // TODO set payment status in merchant's database to 'Success'
          echo "Transaction order_id: " . $order_id . " successfully captured using " . $type;
        }
      }
    } else if ($transaction == 'settlement') {
      // trabnsaksi success
      $this->db->set('status', '2')
        ->where('no_faktur',  $order_id)
        ->update('boking');
    } else if ($transaction == 'pending') {
      // transaksi pending  
      $this->db->set('status', '1')
        ->where('no_faktur',  $order_id)
        ->update('boking');
    } else if ($transaction == 'deny') {
      $this->db->set('status', '0')
        ->where('no_faktur',  $order_id)
        ->update('boking');
    }
  }
}
