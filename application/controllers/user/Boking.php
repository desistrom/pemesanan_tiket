<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boking extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('user/M_boking', 'boking');

    $id_user = $this->session->userdata('id_user');

    if (!$id_user) {
      redirect(base_url('login?boking=true'));
    }
  }

  function index()
  {
    $data['rute'] = $this->db->get_where('rute', ['status' => 'on']);
    $data['kabin'] = $this->db->get_where('kabin', ['status' => 'on']);

    $this->template->load('user/template', 'user/boking', $data);
  }

  function boking_tiket()
  {
    $this->boking->rules_boking_tiket();

    if ($this->form_validation->run()) {
      $string_tgl = $this->input->post('tgl');
      $usia       = $this->input->post('usia');
      $nama       = $this->input->post('nama');
      $phone       = $this->input->post('phone');
      $id_rute    = $this->input->post('id_rute');
      $id_kapal   = $this->input->post('id_kapal');
      $id_kursi   = $this->input->post('id_kursi');
      $kursi      = [];
      $total      = 0;

      $array_date_time = explode(" ", $string_tgl);
      $tgl = explode("-", $array_date_time[0]);

      // array push usia dan kursi
      foreach ($usia as $index => $usa) {
        $kursi[] = [
          'id_kursi' => $id_kursi[$index],
          'nama'     => $nama[$index],
          'phone'   => $phone[$index],
          'usia' => $usa,
        ];
      }

      $tarif = $this->db->get_where('rute', ['id_rute' => $id_rute])->row();


      foreach ($usia as $vu) {
        $vu == 'dewasa' ? $total =  $total + $tarif->tarif_dewasa : $total =  $total + $tarif->tarif_anak;
      }

      $id_user       = $this->session->userdata('id_user');
      $no_faktur     = $this->boking->get_nofak();
      $tgl_boking    = date('Y-m-d H:i:s');
      $tgl_pelayaran = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0] . ' ' . $array_date_time[1];
      $status        = '1';


      $data = [
        'id_user'       => $id_user,
        'id_rute'       => $id_rute,
        'id_kapal'      => $id_kapal,
        'no_faktur'     => $no_faktur,
        'total'         => $total,
        'tgl_pelayaran' => $tgl_pelayaran,
        'tgl_boking'    => $tgl_boking,
        'status'        => $status,
      ];


      // insert table boking
      $this->db->insert('boking', $data);

      $id_boking = $this->db->insert_id();
      // insert table detail_boking
      foreach ($kursi as $vk) {

        $data = [
          'id_boking'     => $id_boking,
          'nama_penerima' => $vk['nama'],
          'phone'         => $vk['phone'],
          'id_kursi'      => $vk['id_kursi'],
          'usia'          => $vk['usia']
        ];

        $this->db->insert('detail_boking', $data);
      }

      $response = [
        'success' => true,
        'no_faktur' => $no_faktur
      ];
      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'phone'   => form_error('phone[]'),
        'nama'   => form_error('nama[]'),
        'id_kursi'   => form_error('id_kursi[]'),
        'tgl'   => form_error('tgl'),
      ];
      echo json_encode($response);
    }
  }

  function cek_jadwal()
  {
    $this->form_validation->set_rules('id', 'id rute', 'required');

    if ($this->form_validation->run()) {

      $id_rute = $this->input->post('id');
      $hari    = $this->input->post('hari');

      $jadwal = $this->boking->get_jadwal($hari, $id_rute);
      if ($jadwal->num_rows() > 0) {
        $response = [
          'success' => true,
          'data'    => $jadwal->row_array()
        ];
      } else {
        $response = [
          'success' => true,
          'data'    => null
        ];
      }

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'hari'    => form_error('hari'),
        'id'      => form_error('id')
      ];

      echo json_encode($response);
    }
  }

  function cek_kursi()
  {

    $this->form_validation->set_rules('id', 'id rute', 'required');
    $this->form_validation->set_rules('tgl', 'tanggal', 'required');
    $this->form_validation->set_rules('kabin', 'Kabin', 'required');
    $this->form_validation->set_rules('usia', 'Usia', 'required');
    $this->form_validation->set_rules('kapal', 'Kapal', 'required');

    if ($this->form_validation->run()) {
      $id_rute = $this->input->post('id');
      $tgl     = $this->input->post('tgl');
      $kabin   = $this->input->post('kabin');
      $kapal   = $this->input->post('kapal');
      $usia    = $this->input->post('usia');

      $rute = $this->db->get_where('rute', ['id_rute' => $id_rute]);

      if ($rute->num_rows() > 0) {
        $get_kursi = $this->boking->get_kursi_boking($id_rute, $tgl);

        // mengecek kursi yang sudah di boking
        $kursi    = null;
        $data     = null;

        // jika kursi ada 
        if ($get_kursi->num_rows() > 0) {
          $kursi    = $get_kursi->result();
          $id_kursi = [];
          // array push;
          foreach ($kursi as $v) {
            $id_kursi[] = (int) $v->id_kursi;
          }

          // mengambil kursi yang belum di boking
          $data = $this->boking->get_kursi($id_kursi, $kabin, $kapal);
        } else {
          $data = $this->db->get_where('kursi', [
            'id_kabin' => $kabin,
            'id_kapal' => $kapal
          ]);
        }

        $response = null;
        $tarif = $usia == 'dewasa' ? $rute->row()->tarif_dewasa : $rute->row()->tarif_anak;

        if ($data->num_rows() > 0) {
          $response = [
            'success' => true,
            'tarif'   => $tarif,
            'data'    => $data->result_array(),
          ];
        } else {
          $response = [
            'success' => true,
            'data'    => null,
          ];
        }

        echo json_encode($response);
      } else if ($rute->num_rows() < 1) {
        $response = [
          'success' => false,
          'rute'    => 'rute tidak tersedia',
        ];

        echo json_encode($response);
      }
    } else {
      $response = [
        'success' => false,
        'rute'    => 'mohon memasukkan semua form'
      ];

      echo json_encode($response);
    }
  }
}
