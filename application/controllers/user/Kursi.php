<?php

class Kursi extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('user/M_kursi', 'kursi');
  }
  function index()
  {
    $id_kapal     = $this->input->post('id_kapal');
    $data['date'] = $this->input->post('tgl');

    !$id_kapal ? $id_kapal = 3 : $id_kapal = $id_kapal;
    !$data['date'] ? $data['date'] = date('d-M-Y') : $data['date'] = $data['date'];

    $data['model'] = $this->kursi;
    $data['kapal'] = $this->db->get('kapal');
    $data['kursi_1'] = $this->kursi->get_kursi(1, $id_kapal);
    $data['kursi_2'] = $this->kursi->get_kursi(2, $id_kapal);
    $data['kursi_3'] = $this->kursi->get_kursi(3, $id_kapal);
    $this->template->load('user/template', 'user/kursi', $data);
  }
}
