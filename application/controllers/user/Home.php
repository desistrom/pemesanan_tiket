<?php


class Home extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->template->load('user/template', 'user/home');
    }
}
