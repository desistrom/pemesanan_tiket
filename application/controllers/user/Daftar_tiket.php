<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Daftar_tiket extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');

    if (!$id_user) {
      redirect(base_url('home'));
    }

    $this->load->model('user/M_daftar_tiket', 'daftar');
  }

  function index()
  {

    $this->template->load('user/template', 'user/daftar_tiket');
  }

  function show()
  {
    $id_user = $this->session->userdata('id_user');

    $list = $this->daftar->get_datatables($id_user);
    $data = array();
    $no = $_POST['start'];
    $status = null;
    foreach ($list as $field) {
      $transaksi = $this->daftar->get_status_result((string) $field->no_faktur);
      $response = json_decode($transaksi, true);

      $status = status_code(
        $response['status_code'],
        $response['status_code'] == '404'  ||  $response['status_code'] == '500'  ? 'error' : $response['transaction_status'],
        $field->status,
        $field->no_faktur
      );

      $row = array();
      $row[] = $field->no_faktur;
      $row[] = $field->nama_kapal;
      $row[] = $field->tempat . '-' . $field->tujuan;
      $row[] = $field->tgl_pelayaran;
      $row[] = $status;
      $row[] = '<a class="btn btn-outline-primary btn-sm" href=' . base_url('invoice/') . $field->no_faktur . '>Detail</a>';
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->daftar->count_all(),
      "recordsFiltered" => $this->daftar->count_filtered($id_user),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  function konfirmasi_pembayaran()
  {
    // resize image
    $tmp_name                       = $_FILES['file']['tmp_name'];
    $this->resize($tmp_name);

    // setting upload file
    $nama                           = $this->input->post('nama');
    $id                             = $this->input->post('id');
    $id_user                        = $this->session->userdata('id_user');
    $tgl                            = date('Y-m-d H:i:s');

    $config['upload_path']          = './assets/image/';
    $config['allowed_types']        = 'jpg|png';
    $config['remove_spaces']        = TRUE;
    $config['max_size']             = 5000;
    $config['max_width']            = 2024;
    $config['max_height']           = 1068;

    $this->load->library('upload', $config);

    // mengecek error 
    if (!$this->upload->do_upload('file') || $nama == null || $id == null) {

      $response = [
        'success' => false,
        'id'    => $nama ? '' : 'this id is Required!!',
        'nama'    => $id ? '' : 'this Nama is Required!!',
        'file'    =>  $this->upload->display_errors()
      ];

      echo json_encode($response);
    } else {
      $data = $this->upload->data();
      // insert table konfirmasi pembayaran
      $data = [
        'id_boking' => $id,
        'nama'      => $nama,
        'tgl'       => $tgl,
        'gambar'    => $data['file_name']
      ];

      $this->db->insert('konfirmasi_pembayaran', $data);
      // update status
      $this->db->where('id_boking', $id)
        ->update('boking', ['status' => '2']);
      // get banyak tiket yang status 1

      $jumlah_tiket = $this->db->get_where('boking', [
        'id_user' => $id_user,
        'status'    => '1'
      ])->num_rows();


      $response = [
        'success' => true,
        'jumlah_tiket' => $jumlah_tiket
      ];

      echo json_encode($response);
    }
  }

  function batal_boking()
  {
    $this->form_validation->set_rules('id', 'Id', 'required');

    if ($this->form_validation->run()) {
      $id      = $this->input->post('id');
      $id_user = $this->session->userdata('id_user');

      $this->db->where('id_boking', $id)
        ->update('boking', ['status' => '0']);
      // get banyak tiket yang status 1

      $jumlah_tiket = $this->db->get_where('boking', [
        'id_user' => $id_user,
        'status'    => '1'
      ])->num_rows();

      $response = [
        'success' => true,
        'jumlah_tiket'  => $jumlah_tiket
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'id'      => form_error('id'),
      ];

      echo json_encode($response);
    }
  }

  function resize($path)
  {

    $config['image_library'] = 'gd2';
    $config['source_image'] = $path;
    $config['create_thumb'] = FALSE;
    $config['maintain_ratio'] = TRUE;
    $config['width']         = 200;
    $config['height']       = 175;

    $this->load->library('image_lib', $config);

    $this->image_lib->resize();
  }
}
