<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->load->model('user/M_jadwal', 'jadwal');
  }

  function index()
  {
    $data['hari'] = $this->jadwal->get_hari();
    $this->template->load('user/template', 'user/jadwal', $data);
  }
}
