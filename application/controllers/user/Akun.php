<?php

class Akun extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $id_user = $this->session->userdata('id_user');

    if (!$id_user) {
      redirect(base_url('home'));
    }

    $this->load->model('user/M_akun', 'akun');
  }

  function index()
  {
    $id_user = $this->session->userdata('id_user');

    $data['user'] = $this->db->get_where('user', ['id_user' => $id_user]);

    $this->template->load('user/template', 'user/akun', $data);
  }

  function update_user()
  {
    $id_user = $this->session->userdata('id_user');
    $id        = $this->input->post('id');

    if ($id_user == $id) {
      $this->akun->rules_update();
      $password_user    = $this->input->post('password');
      $confirm_password = $this->akun->get_password($id_user);

      $password = password_verify($password_user, $confirm_password->password);

      if ($password) {
        if ($this->form_validation->run()) {
          $nama      = $this->input->post('nama');
          $alamat    = $this->input->post('alamat');
          $tgl_lahir = $this->input->post('tgl_lahir') ? $this->input->post('tgl_lahir') : null;
          $genre     = $this->input->post('genre') ? $this->input->post('genre') : null;

          if ($tgl_lahir) {
            $array_tgl = explode('-', $tgl_lahir);
            $tgl_lahir = $array_tgl[2] . '-' . $array_tgl[1] . '-' . $array_tgl[0];
          }

          $data = [
            'nama' => $nama,
            'alamat' => $alamat,
            'tgl_lahir' => $tgl_lahir,
            'genre' => $genre,
          ];

          $this->db->where(['id_user' => $id])
            ->update('user', $data);

          $response = [
            'success' => true
          ];

          echo json_encode($response);
        } else {
          $response = [
            'success' => false,
            'nama'    => form_error('nama'),
            'alamat'    => form_error('alamat'),
            'tgl_lahir'    => form_error('tgl_lahir'),
            'genre'    => form_error('genre'),

          ];

          echo json_encode($response);
        }
      } else {
        $response = [
          'success' => false,
          'alert'   => 'Password yang anda masukkan salah'
        ];

        echo json_encode($response);
      }
    } else {
      $response = [
        'success' => false,
        'alert'   => 'id update user salah'
      ];

      echo json_encode($response);
    }
  }
}
