<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kota extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);

    $this->load->model('admin/M_kota', 'kota');
  }

  function index()
  {
    $data['rute'] = $this->db->get('rute');
    $this->template->load('admin/template', 'admin/kota', $data);
  }

  public function show()
  {
    $list = $this->kota->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $field) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $field->rute;
      $row[] = $field->kota;
      $row[] = $field->jarak == '-' ? '(Selesai)' : $field->jarak . ' KM';
      $row[] = $field->posisi;
      $row[] = '<button class="btn btn-outline-primary edit btn-sm" id=' . $field->id_kota . '>Edit</button>';
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->kota->count_all(),
      "recordsFiltered" => $this->kota->count_filtered(),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  function insert()
  {
    $this->kota->rules_insert();

    if ($this->form_validation->run()) {

      $id_rute   = $this->input->post('id_rute', true);
      $tarif     = $this->input->post('kota', true);
      $jarak     = $this->input->post('jarak', true);
      $posisi    = $this->input->post('posisi', true);

      $data = [
        'id_rute'    => $id_rute,
        'kota'      => $tarif,
        'jarak'      => $jarak,
        'posisi'     => $posisi,
      ];

      $this->db->insert('kota', $data);

      $response = [
        'success' => true,
        'insert'  => true,
        'alert'   => 'berhasil tambah data',
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'insert'  => false,
        'id_rute' => form_error('id_rute'),
        'kota'    => form_error('kota'),
        'jarak'   => form_error('jarak'),
        'posisi'  => form_error('posisi'),
      ];

      echo json_encode($response);
    }
  }

  function show_data()
  {
    $this->form_validation->set_rules('id', 'ID', 'required');

    if ($this->form_validation->run()) {
      $id = $this->input->post('id');

      $data = $this->db->from('kota')
        ->join('rute', 'kota.id_rute = rute.id_rute')
        ->where(['id_kota' => $id])->get()->row_array();

      $response = [
        'success' => true,
        'data'    => $data,
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'id'      => form_error('id'),
      ];

      echo json_encode($response);
    }
  }

  function update()
  {
    $this->kota->rules_update();

    if ($this->form_validation->run()) {

      $id      = $this->input->post('id', true);
      $id_rute = $this->input->post('id_rute', true);
      $kota    = $this->input->post('kota', true);
      $jarak   = $this->input->post('jarak', true);
      $posisi  = $this->input->post('posisi', true);

      $data = [
        'id_rute' => $id_rute,
        'kota'    => $kota,
        'jarak'   => $jarak,
        'posisi'  => $posisi,
      ];

      $this->db->where('id_kota', $id)
        ->update('kota', $data);

      $response = [
        'success' => true,
        'update'  => true,
        'alert'   => 'berhasil update data',

      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'update'  => false,
        'id'      => form_error('id'),
        'id_rute' => form_error('id_rute'),
        'kota'    => form_error('kota'),
        'jarak'   => form_error('jarak'),
        'posisi'  => form_error('posisi'),
      ];

      echo json_encode($response);
    }
  }
}
