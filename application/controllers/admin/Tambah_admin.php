<?php

class Tambah_admin extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);
    $this->load->model('admin/M_tambah_admin', 'admin');
  }

  function index()
  {
    $this->template->load('admin/template', 'admin/tambah_admin');
  }

  public function show()
  {
    $list = $this->admin->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $field) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $field->email;
      $row[] = $field->nama;
      $row[] = $field->alamat;
      $row[] = $field->level == '1' ? 'Admin' : 'User';
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->admin->count_all(),
      "recordsFiltered" => $this->admin->count_filtered(),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  public function insert()
  {
    $this->admin->rules_insert();

    if ($this->form_validation->run()) {
      $email     = $this->input->post('email', true);
      $nama      = $this->input->post('nama', true);
      $alamat    = $this->input->post('alamat', true);
      $level     = '1';
      $password  = $this->input->post('password', true);

      $data = [
        'email'    => $email,
        'password' => password_hash($password, PASSWORD_DEFAULT),
        'nama'     => $nama,
        'alamat'   => $alamat,
        'level'    => $level,
      ];

      $this->db->insert('user', $data);

      $response = [
        'success' => true,
        'alert'   => 'Berhasil Tambah admin'
      ];
      echo json_encode($response);
    } else {
      $response = [
        'success'   => false,
        'email'     => form_error('email'),
        'nama'      => form_error('nama'),
        'alamat'    => form_error('alamat'),
        'password'  => form_error('password'),
        'password2' => form_error('password2'),
      ];

      echo json_encode($response);
    }
  }
}
