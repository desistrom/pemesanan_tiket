<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rute extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);

    $this->load->model('admin/M_rute', 'rute');
  }

  function index()
  {
    $this->template->load('admin/template', 'admin/rute');
  }

  public function show()
  {
    $list = $this->rute->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $field) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $field->tempat;
      $row[] = $field->tujuan;
      $row[] = 'Rp' . number_format($field->tarif_anak);
      $row[] = 'Rp' . number_format($field->tarif_dewasa);
      $row[] = $field->jarak . ' KM';
      $row[] = $field->status;
      $row[] = '<button class="btn btn-outline-primary edit btn-sm" id=' . $field->id_rute . '>Edit</button>';
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->rute->count_all(),
      "recordsFiltered" => $this->rute->count_filtered(),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  function insert()
  {
    $this->rute->rules_insert();

    if ($this->form_validation->run()) {
      $tempat   = $this->input->post('tempat', true);
      $tujuan   = $this->input->post('tujuan', true);
      $tarif_dewasa  = $this->input->post('tarif_dewasa', true);
      $tarif_anak  = $this->input->post('tarif_anak', true);
      $jarak  = $this->input->post('jarak', true);
      $status = $this->input->post('status', true);

      $data = [
        'tempat'    => $tempat,
        'tujuan'    => $tujuan,
        'tarif_dewasa'   => $tarif_dewasa,
        'tarif_anak'   => $tarif_anak,
        'jarak'   => $jarak,
        'status'  => $status,
      ];

      $this->db->insert('rute', $data);

      $response = [
        'success' => true,
        'insert'  => true,
        'alert'   => 'berhasil tambah data',
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'insert'  => false,
        'tempat'    => form_error('tempat'),
        'tujuan'    => form_error('tujuan'),
        'tarif_dewasa'   => form_error('tarif_dewasa'),
        'tarif_anak'   => form_error('tarif_anak'),
        'jarak'   => form_error('jarak'),
        'status'   => form_error('status'),
      ];

      echo json_encode($response);
    }
  }

  function show_data()
  {
    $this->form_validation->set_rules('id', 'ID', 'required');

    if ($this->form_validation->run()) {
      $id = $this->input->post('id');

      $data = $this->db->get_where('rute', ['id_rute' => $id])->row_array();

      $response = [
        'success' => true,
        'data'    => $data,
      ];
      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'id'      => form_error('id'),
      ];

      echo json_encode($response);
    }
  }

  function update()
  {
    $this->rute->rules_update();

    if ($this->form_validation->run()) {
      $id     = $this->input->post('id', true);
      $tempat = $this->input->post('tempat', true);
      $tujuan = $this->input->post('tujuan', true);
      $tarif_dewasa  = $this->input->post('tarif_dewasa', true);
      $tarif_anak  = $this->input->post('tarif_anak', true);
      $jarak  = $this->input->post('jarak', true);
      $status = $this->input->post('status', true);

      $data = [
        'tempat'    => $tempat,
        'tujuan'    => $tujuan,
        'tarif_dewasa'     => $tarif_dewasa,
        'tarif_anak'     => $tarif_anak,
        'jarak'     => $jarak,
        'status'    => $status,
      ];

      $this->db->where('id_rute', $id)
        ->update('rute', $data);

      $response = [
        'success' => true,
        'update'  => true,
        'alert'   => 'berhasil update data',

      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'update'  => false,
        'tempat'    => form_error('tempat'),
        'jarak'    => form_error('jarak'),
        'tarif_anak'   => form_error('tarif_anak'),
        'tarif_dewasa'   => form_error('tarif_dewasa'),
        'tujuan'   => form_error('tujuan'),
        'status'  => form_error('status'),
      ];

      echo json_encode($response);
    }
  }
}
