<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Boking extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);

    $this->load->model('admin/M_boking', 'boking');
    $this->load->model('user/M_daftar_tiket', 'daftar');
  }

  function index()
  {
    $data['rute'] = $this->db->get_where('rute');
    $this->template->load('admin/template', 'admin/boking', $data);
  }

  public function show()
  {
    $list = $this->boking->get_datatables();
    $data = array();
    $no = $_POST['start'];
    $status = null;

    foreach ($list as $field) {

      // $status = status_code(
      //   $response['status_code'],
      //   $response['status_code'] == '404'  ||  $response['status_code'] == '500'  ? 'error' : $response['transaction_status'],
      //   $field->status,
      //   $field->no_faktur
      // );

      $status = '';

      if ($field->status == '1') {
        $status = '<div class="badge badge-warning">transaksi pending</div>';
      } else if ($field->status == '2') {
        $status = '<div class="badge badge-success">transaksi success</div>';
      } else if ($field->status == '0') {
        $status = '<div class="badge badge-danger">transaksi batal</div>';
      }

      $no++;
      $row = array();
      $row[] = $field->no_faktur;
      $row[] = $field->nama;
      $row[] = $field->tempat . '-' . $field->tujuan;
      $row[] = $field->tgl_pelayaran;
      $row[] = $field->tgl_boking;
      $row[] = $status;
      $row[] = '<div class="dropdown d-inline-block">
                  <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary btn-sm">Action</button>
                  <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                      <a href="' . base_url('invoice/') . $field->no_faktur . '" class="dropdown-item" id=' . $field->id_boking . '>Detail</a>
                      <button type="button" tabindex="0" class="dropdown-item btnUpdateStatus" id=' . $field->id_boking . ' data-toggle="modal" data-target="#exampleModal">Update status</button>
                  </div>
              </div>';
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->boking->count_all(),
      "recordsFiltered" => $this->boking->count_filtered(),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  function update_status()
  {
    $valid = $this->form_validation;
    $valid->set_rules('id_boking', 'id boking', 'required');
    $valid->set_rules('status', 'status', 'required|numeric');

    if ($valid->run()) {
      $id_boking = $this->input->post('id_boking', true);
      $status    = $this->input->post('status', true);

      $this->db->set('status', $status)
        ->where('id_boking', $id_boking)
        ->update('boking');

      $response = [
        'success' => true,

      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'id_boking' => form_error('id_boking'),
        'status' => form_error('status')
      ];

      echo json_encode($response);
    }
  }
}
