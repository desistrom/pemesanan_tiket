<?php

class Laporan extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);

    $this->load->model('admin/M_laporan', 'laporan');
  }

  function index()
  {
    $this->template->load('admin/template', 'admin/laporan');
  }

  function hari()
  {
    $form_valid = $this->form_validation;
    $form_valid->set_rules('value', 'Tanggal', 'required');

    if ($form_valid->run()) {
      $tgl = $this->input->post('value', true);
      $id_user = $this->session->userdata('id_user');

      $data['laporan'] = $this->laporan->get_perhari($tgl);

      if ($data['laporan']->num_rows() > 0) {
        $data['total']   = $this->laporan->get_total_hari($tgl)->row();

        $data['tgl'] = $tgl;
        $data['user']    = $this->db->get_where('user', ['id_user' => $id_user])->row();

        $this->template->load('admin/template', 'admin/laporan/hari', $data);
      } else {
        $this->session->set_flashdata('msg', 'Data pada tanggal ' . $tgl . ' tidak tersedia');
        redirect(base_url('laporan'));
      }
    } else {
      $this->session->set_flashdata('msg', 'Harap mengisi form yang tersedia');
      redirect(base_url('laporan'));
    }
  }

  function bulan()
  {
    $form_valid = $this->form_validation;
    $form_valid->set_rules('value', 'Bulan', 'required');

    if ($form_valid->run()) {
      $tgl = $this->input->post('value', true);
      $id_user = $this->session->userdata('id_user');

      $data['laporan'] = $this->laporan->get_bulan($tgl);
      if ($data['laporan']->num_rows() > 0) {
        $data['total'] = $this->laporan->get_total_bulan($tgl)->row();

        $data['bulan'] = $tgl;
        $data['user']    = $this->db->get_where('user', ['id_user' => $id_user])->row();

        $this->template->load('admin/template', 'admin/laporan/bulan', $data);
      } else {
        $this->session->set_flashdata('msg', 'Data pada tanggal ' . $tgl . ' tidak tersedia');
        redirect(base_url('laporan'));
      }
    } else {
      $this->session->set_flashdata('msg', 'Harap mengisi form yang tersedia');
      redirect(base_url('laporan'));
    }
  }
}
