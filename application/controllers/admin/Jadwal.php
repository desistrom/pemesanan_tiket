<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);

    $this->load->model('admin/M_jadwal', 'jadwal');
  }

  function index()
  {
    $data['rute'] = $this->db->get('rute');
    $data['kapal'] = $this->db->get('kapal');
    $this->template->load('admin/template', 'admin/jadwal', $data);
  }

  public function show()
  {
    $list = $this->jadwal->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $field) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $field->nama_kapal;
      $row[] = $field->tempat;
      $row[] = $field->tujuan;
      $row[] = $field->jam;
      $row[] = '<button class="btn btn-outline-primary edit btn-sm" id=' . $field->id_jadwal . '>Edit</button>';
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->jadwal->count_all(),
      "recordsFiltered" => $this->jadwal->count_filtered(),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  function insert()
  {
    $this->jadwal->rules_insert();

    if ($this->form_validation->run()) {
      $id_rute   = $this->input->post('id_rute', true);
      $id_kapal   = $this->input->post('id_kapal', true);
      $hari      = $this->input->post('hari', true);
      $jam       = $this->input->post('jam', true);

      $data = [
        'id_rute' => $id_rute,
        'id_kapal' => $id_kapal,
        'hari'    => $hari,
        'jam'     => $jam,
      ];

      $this->db->insert('jadwal', $data);

      $response = [
        'success' => true,
        'insert'  => true,
        'alert'   => 'berhasil tambah data',
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'insert'  => false,
        'id_rute' => form_error('id_rute'),
        'id_kapal' => form_error('id_kapal'),
        'hari'    => form_error('hari'),
        'jam'     => form_error('jam'),
      ];

      echo json_encode($response);
    }
  }

  function show_data()
  {
    $this->form_validation->set_rules('id', 'ID', 'required');

    if ($this->form_validation->run()) {
      $id = $this->input->post('id');
      $data = $this->db->get_where('jadwal', ['id_jadwal' => $id])->row_array();

      $response = [
        'success' => true,
        'data'    => $data,
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'id'      => form_error('id'),
      ];

      echo json_encode($response);
    }
  }

  function update()
  {
    $this->jadwal->rules_update();

    if ($this->form_validation->run()) {
      $id      = $this->input->post('id', true);

      $id_rute  = $this->input->post('id_rute',  true);
      $id_kapal = $this->input->post('id_kapal', true);
      $hari     = $this->input->post('hari', true);
      $jam      = $this->input->post('jam', true);

      $data = [
        'id_rute'  => $id_rute,
        'id_kapal' => $id_kapal,
        'hari'    => $hari,
        'jam'     => $jam,
      ];

      $this->db->where('id_jadwal', $id)
        ->update('jadwal', $data);

      $response = [
        'success' => true,
        'update'  => true,
        'alert'   => 'berhasil update data',

      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'update'  => false,
        'id'    => form_error('id'),
        'id_rute'   => form_error('id_rute'),
        'id_kapal'   => form_error('id_kapal'),
        'hari'  => form_error('hari'),
        'jam'  => form_error('jam'),
      ];

      echo json_encode($response);
    }
  }
}
