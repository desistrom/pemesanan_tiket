<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kabin extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);

    $this->load->model('admin/M_kabin', 'kabin');
  }

  function index()
  {
    $this->template->load('admin/template', 'admin/kabin');
  }

  public function show()
  {
    $list = $this->kabin->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $field) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $field->kabin;
      $row[] = $field->status;
      $row[] = '<button class="btn btn-outline-primary edit btn-sm" id=' . $field->id_kabin . '>Edit</button>';
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->kabin->count_all(),
      "recordsFiltered" => $this->kabin->count_filtered(),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  function insert()
  {
    $this->kabin->rules_insert();

    if ($this->form_validation->run()) {
      $kabin   = $this->input->post('kabin', true);
      $status  = $this->input->post('status', true);

      $data = [
        'kabin'    => $kabin,
        'status'   => $status,
      ];

      $this->db->insert('kabin', $data);

      $response = [
        'success' => true,
        'insert'  => true,
        'alert'   => 'berhasil tambah data',
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'insert'  => false,
        'kabin'    => form_error('kabin'),
        'status'   => form_error('status'),
      ];

      echo json_encode($response);
    }
  }

  function show_data()
  {
    $this->form_validation->set_rules('id', 'ID', 'required');

    if ($this->form_validation->run()) {
      $id = $this->input->post('id');

      $data = $this->db->get_where('kabin', ['id_kabin' => $id])->row_array();

      $response = [
        'success' => true,
        'data'    => $data,
      ];
      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'id'      => form_error('id'),
      ];

      echo json_encode($response);
    }
  }

  function update()
  {

    $this->kabin->rules_update();

    if ($this->form_validation->run()) {
      $id     = $this->input->post('id', true);
      $kabin  = $this->input->post('kabin', true);
      $status = $this->input->post('status', true);

      $data = [
        'kabin'    => $kabin,
        'status'  => $status,
      ];

      $this->db->where('id_kabin', $id)
        ->update('kabin', $data);

      $response = [
        'success' => true,
        'update'  => true,
        'alert'   => 'berhasil update data',

      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'update'  => false,
        'id'    => form_error('id'),
        'kabin'   => form_error('kabin'),
        'status'  => form_error('status'),
      ];

      echo json_encode($response);
    }
  }
}
