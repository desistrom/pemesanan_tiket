<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kursi extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);

    $this->load->model('admin/M_kursi', 'kursi');
  }

  function index()
  {
    $data['kabin'] = $this->db->get_where('kabin', ['status' => 'on']);
    $data['kapal'] = $this->db->get_where('kapal', ['status' => 'on']);
    $this->template->load('admin/template', 'admin/kursi', $data);
  }

  public function show()
  {
    $list = $this->kursi->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $field) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $field->kabin;
      $row[] = $field->nama_kapal;
      $row[] = $field->kursi;
      $row[] = $field->status;
      $row[] = '<button class="btn btn-outline-primary edit btn-sm" id=' . $field->id_kursi . '>Edit</button>';
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->kursi->count_all(),
      "recordsFiltered" => $this->kursi->count_filtered(),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  function insert()
  {
    $this->kursi->rules_insert();

    if ($this->form_validation->run()) {
      $id_kabin   = $this->input->post('id_kabin', true);
      $id_kapal   = $this->input->post('id_kapal', true);
      $kursi   = $this->input->post('kursi', true);
      $status  = $this->input->post('status', true);

      $data = [
        'id_kabin' => $id_kabin,
        'id_kapal' => $id_kapal,
        'kursi'    => $kursi,
        'status'   => $status,
      ];

      $this->db->insert('kursi', $data);

      $response = [
        'success' => true,
        'insert'  => true,
        'alert'   => 'berhasil tambah data',
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'insert'  => false,
        'id_kabin'    => form_error('id_kabin'),
        'id_kapal'    => form_error('id_kapal'),
        'kursi'    => form_error('kursi'),
        'status'   => form_error('status'),
      ];

      echo json_encode($response);
    }
  }

  function show_data()
  {
    $this->form_validation->set_rules('id', 'ID', 'required');

    if ($this->form_validation->run()) {
      $id = $this->input->post('id');

      $data = $this->db->select('kapal.nama as nama_kapal, kursi,kursi.id_kabin as id_kabin, id_kursi,kursi.status as status')
        ->join('kapal', 'kursi.id_kapal = kapal.id_kapal')
        ->join('kabin', 'kursi.id_kabin = kabin.id_kabin')
        ->from('kursi')
        ->where([
          'id_kursi' => $id
        ])->get()->row_array();

      $response = [
        'success' => true,
        'data'    => $data,
      ];
      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'id'      => form_error('id'),
      ];

      echo json_encode($response);
    }
  }

  function update()
  {
    $this->kursi->rules_update();

    if ($this->form_validation->run()) {
      $id     = $this->input->post('id', true);
      $id_kabin  = $this->input->post('id_kabin', true);
      $id_kapal  = $this->input->post('id_kapal', true);
      $kursi  = $this->input->post('kursi', true);
      $status = $this->input->post('status', true);

      $data = [
        'id_kabin'    => $id_kabin,
        'id_kapal'    => $id_kapal,
        'kursi'    => $kursi,
        'status'  => $status,
      ];

      $this->db->where('id_kursi', $id)
        ->update('kursi', $data);

      $response = [
        'success' => true,
        'update'  => true,
        'alert'   => 'berhasil update data',

      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'update'  => false,
        'id'    => form_error('id'),
        'id_kabin'   => form_error('id_kabin'),
        'id_kapal'   => form_error('id_kapal'),
        'kursi'   => form_error('kursi'),
        'status'  => form_error('status'),
      ];

      echo json_encode($response);
    }
  }
}
