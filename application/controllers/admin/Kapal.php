<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kapal extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');

    $this->login_verify->cek_admin($id_user, $level);

    $this->load->model('admin/M_kapal', 'kapal');
  }

  function index()
  {
    $this->template->load('admin/template', 'admin/kapal');
  }

  public function show()
  {
    $list = $this->kapal->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $field) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $field->nama;
      $row[] = $field->status;
      $row[] = '<button class="btn btn-outline-primary edit btn-sm" id=' . $field->id_kapal . '>Edit</button>';
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->kapal->count_all(),
      "recordsFiltered" => $this->kapal->count_filtered(),
      "data" => $data,
    );

    echo json_encode($output); //output dalam format JSON
  }

  function insert()
  {
    $this->kapal->rules_insert();

    if ($this->form_validation->run()) {
      $kapal   = $this->input->post('kapal', true);
      $status  = $this->input->post('status', true);

      $data = [
        'nama'    => $kapal,
        'status'   => $status,
      ];

      $this->db->insert('kapal', $data);

      $response = [
        'success' => true,
        'insert'  => true,
        'alert'   => 'berhasil tambah data',
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'insert'  => false,
        'nama'    => form_error('kapal'),
        'status'   => form_error('status'),
      ];

      echo json_encode($response);
    }
  }

  function show_data()
  {
    $this->form_validation->set_rules('id', 'ID', 'required');

    if ($this->form_validation->run()) {
      $id = $this->input->post('id');

      $data = $this->db->get_where('kapal', ['id_kapal' => $id])->row_array();

      $response = [
        'success' => true,
        'data'    => $data,
      ];
      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'id'      => form_error('id'),
      ];

      echo json_encode($response);
    }
  }

  function update()
  {

    $this->kabin->rules_update();

    if ($this->form_validation->run()) {
      $id     = $this->input->post('id', true);
      $kapal  = $this->input->post('kapal', true);
      $status = $this->input->post('status', true);

      $data = [
        'nama'    => $kapal,
        'status'  => $status,
      ];

      $this->db->where('id_kapal', $id)
        ->update('kapal', $data);

      $response = [
        'success' => true,
        'update'  => true,
        'alert'   => 'berhasil update data',

      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'update'  => false,
        'id'    => form_error('id'),
        'nama'   => form_error('kapal'),
        'status'  => form_error('status'),
      ];

      echo json_encode($response);
    }
  }
}
