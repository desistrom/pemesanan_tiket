<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Register extends CI_Controller
{


  function __construct()
  {
    parent::__construct();

    $id_user = $this->session->userdata('id_user');
    $level   = $this->session->userdata('level');
    $this->login_verify->cek_login($id_user, $level);
    $this->load->model('auth/M_register', 'register');
  }

  function index()
  {
    $this->template->load('auth/template', 'auth/register');
  }

  function registrasi()
  {
    $this->register->rules_register();

    if ($this->form_validation->run()) {

      $level     = 2;
      $nama      = $this->input->post('nama');
      $email     = $this->input->post('email');
      $alamat    = $this->input->post('alamat');
      $password  = $this->input->post('password');

      $data = [
        'nama'     => $nama,
        'email'    => $email,
        'alamat'   => $alamat,
        'password' => password_hash($password, PASSWORD_DEFAULT),
        'level'    => $level
      ];

      $this->db->insert('user', $data);

      $this->session->set_flashdata('msg', '<div class="text-center alert alert-success col-12">Registrasi berhasil <span class="icon-check-circle"></span></div>');
      $response = [
        'success' => true,
      ];

      echo json_encode($response);
    } else {
      $response = [
        'success' => false,
        'nama' => form_error('nama', '<p class="text-warning">', '</p>'),
        'email'   => form_error('email', '<p class="text-warning">', '</p>'),
        'alamat'   => form_error('alamat', '<p class="text-warning">', '</p>'),
        'password' => form_error('password', '<p class="text-warning">', '</p>'),
        'password2' => form_error('password2', '<p class="text-warning">', '</p>'),
      ];

      echo json_encode($response);
    }
  }
}
