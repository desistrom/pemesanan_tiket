$(document).ready(function () {
	var table

	table = $('#tblDaftarTiket').DataTable({
		"searching": false,
		"info": false,
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax": {
			"url": base_url + 'user/Daftar_tiket/show',
			"type": "POST"
		},


		"columnDefs": [{
			"targets": [0],
			"orderable": false,
		}, ],
	});

	$(document).on('click', '.konfirmasi', function () {
		const value = $(this).attr('id');

		$('#modalKonfirmasi').modal('show');

		$('#id_boking').val(value);
	});

	$('#formKonfirmasi').on('submit', (function (e) {
		e.preventDefault();
		var formData = new FormData(this);
		$('.form-control').removeClass('is-invalid');
		$('#loader2').removeClass('d-none');

		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: formData,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			success: function (response) {
				$('#loader2').addClass('d-none');

				if (response.success === true) {
					Swal.fire(
						'Berhasil.',
						'Berhasil konfirmasi pembayaran',
						'success',
					)
					table.ajax.reload();
					$('#formKonfirmasi')[0].reset();
					$('#modalKonfirmasi').modal('hide');
					if (response.jumlah_tiket > 0) {
						$('.jumlah-tiket').removeClass('d-none');
						$('.jumlah-tiket').html(response.jumlah_tiket);
					} else if (response.jumlah_tiket < 1) {

						$('.jumlah-tiket').addClass('d-none');
					}
				} else if (response.success === false) {
					if (response.id) {
						$('#nama').addClass('is-invalid');
						$('#errorNama').html(response.nama);
					}
					if (response.file) {
						$('#file').addClass('is-invalid');
						$('#errorFile').html(response.file);
					}
				}
			},
		});
	}));

});
