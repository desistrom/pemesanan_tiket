$(document).ready(function () {
	$('#btnInsert').click(function (e) {
		e.preventDefault();
		$('#idRute option').removeAttr('selected');
		$('#formAction')[0].reset();
		$('#formAction').attr('action', base_url + 'admin/Rute/insert');
		$('.form-control').removeClass('is-invalid');
		$('#data').slideUp();
		$(this).slideUp();
		$('#action').slideDown();
		$('#btnView').slideDown();
	});

	$('#btnView').click(function (e) {
		e.preventDefault();
		$('#action').slideUp();
		$(this).slideUp();
		$('#data').slideDown();
		$('#btnInsert').slideDown();
	});

	$(document).on('click', '.edit', function (e) {
		e.preventDefault();
		const id = $(this).attr('id');

		$.ajax({
			type: "post",
			url: base_url + 'admin/Rute/show_data',
			data: {
				id: id
			},
			dataType: "json",
			success: function (response) {
				const value = response.data;

				if (response.success == false) {
					$('#alert').addClass('alert-danger');
					$('#alert').html('data tidak di temukan');
				} else if (response.success == true) {
					$('#formAction').attr('action', base_url + 'admin/Rute/update');
					$('#idRute').val(value.id_rute);
					$('.form-control').removeClass('is-invalid');
					$('#data').slideUp();
					$('#btnInsert').slideUp();
					$('#action').slideDown();
					$('#btnView').slideDown();

					$('#tempat').val(value.tempat);
					$('#tujuan').val(value.tujuan);
					$('#tarif').val(value.tarif);
					$('#jarak').val(value.jarak);

					$('#status option').filter(function () {
						if (this.value == value.status) {
							$(this).attr('selected', 'selected');
						}
					});
				}

			}
		});
	});

	var datatable;
	datatable = $('#table').DataTable({
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax": {
			"url": base_url + 'admin/Rute/show',
			"type": "POST"
		},


		"columnDefs": [{
			"targets": [0],
			"orderable": false,
		}, ],
	});

	$('#searchDatatable').on('keyup', function () { //Search using custom input box
		datatable.search(this.value).draw();
	});

	$('#closeAlert').click(function (e) {
		e.preventDefault();
		$('#alertAction').addClass('d-none');
	});

	$('#formAction').submit(function (e) {
		e.preventDefault();

		let value = $(this);
		$('.form-control').removeClass('is-invalid');
		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				if (response.success == false) {
					if (response.tempat) {
						$('#tempat').addClass('is-invalid');
						$('#errorTempat').html(response.tempat);
					}
					if (response.tujuan) {
						$('#tujuan').addClass('is-invalid');
						$('#errorTujuan').html(response.tujuan);
					}
					if (response.tarif_dewasa) {
						$('#tarif_dewasa').addClass('is-invalid');
						$('#errorTarifDewasa').html(response.tarif_dewasa);
					}
					if (response.tarif_anak) {
						$('#tarif_anak').addClass('is-invalid');
						$('#errorTarifAnak').html(response.tarif_anak);
					}
					if (response.jarak) {
						$('#jarak').addClass('is-invalid');
						$('#errorJarak').html(response.jarak);
					}
					if (response.status) {
						$('#status').addClass('is-invalid');
						$('#errorStatus').html(response.status);
					}
				} else if (response.success == true) {
					datatable.ajax.reload();
					$('#alert').html(response.alert);
					$('#alertAction').removeClass('d-none');
					$('#btnView').slideUp();
					$('#btnInsert').slideDown();
					$('#action').slideUp();
					$('#data').slideDown();
				}
			}
		});
	});
});
