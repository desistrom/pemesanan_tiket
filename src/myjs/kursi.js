$(document).ready(function () {
	$('#btnInsert').click(function (e) {
		e.preventDefault();
		$('#status option').removeAttr('selected');
		$('#formAction')[0].reset();
		$('#formAction').attr('action', base_url + 'admin/Kursi/insert');
		$('.form-control').removeClass('is-invalid');
		$('#data').slideUp();
		$('#kabin').val("");
		$(this).slideUp();
		$('#action').slideDown();
		$('#btnView').slideDown();
	});

	$('#btnView').click(function (e) {
		e.preventDefault();
		$('#action').slideUp();
		$(this).slideUp();
		$('#data').slideDown();
		$('#btnInsert').slideDown();
	});

	$(document).on('click', '.edit', function (e) {
		e.preventDefault();
		const id = $(this).attr('id');

		$.ajax({
			type: "post",
			url: base_url + 'admin/Kursi/show_data',
			data: {
				id: id
			},
			dataType: "json",
			success: function (response) {
				const value = response.data;

				if (response.success == false) {
					$('#alert').addClass('alert-danger');
					$('#alert').html('data tidak di temukan');
				} else if (response.success == true) {

					$('#formAction').attr('action', base_url + 'admin/Kursi/update');
					$('.form-control').removeClass('is-invalid');
					$('#data').slideUp();
					$('#btnInsert').slideUp();
					$('#action').slideDown();
					$('#btnView').slideDown();
					$('#kabin').val(value.id_kabin);
					$('#idKursi').val(value.id_kursi);
					$('#kursi').val(value.kursi);

					$('#status').val(value.status);
				}

			}
		});
	});

	var datatable;
	datatable = $('#table').DataTable({
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax": {
			"url": base_url + 'admin/Kursi/show',
			"type": "POST"
		},


		"columnDefs": [{
			"targets": [0],
			"orderable": false,
		}, ],
	});

	$('#searchDatatable').on('keyup', function () { //Search using custom input box
		datatable.search(this.value).draw();
	});
	// mengahapus alert
	$('#closeAlert').click(function (e) {
		e.preventDefault();
		$('#alertAction').addClass('d-none');
	});

	$('#formAction').submit(function (e) {
		e.preventDefault();

		let value = $(this);
		$('.form-control').removeClass('is-invalid');
		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				if (response.success == false) {
					if (response.id_kabin) {
						$('#kabin').addClass('is-invalid');
						$('#errorKabin').html(response.id_kabin);
					}
					if (response.id_kapal) {
						$('#kapal').addClass('is-invalid');
						$('#errorKapal').html(response.id_kapal);
					}
					if (response.kursi) {
						$('#kursi').addClass('is-invalid');
						$('#errorKursi').html(response.kursi);
					}
					if (response.status) {
						$('#status').addClass('is-invalid');
						$('#errorStatus').html(response.status);
					}
				} else if (response.success == true) {
					datatable.ajax.reload();
					$('#alert').html(response.alert);
					$('#alertAction').removeClass('d-none');
					$('#btnView').slideUp();
					$('#btnInsert').slideDown();
					$('#action').slideUp();
					$('#data').slideDown();
				}
			}
		});
	});
});
