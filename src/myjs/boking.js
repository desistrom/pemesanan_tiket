$(document).ready(function () {
	let alertErr = $('#alert').val();
	if (alertErr == 'penuh') {
		swal.fire(
			'Pemberitahuan',
			'Kursi sudah penuh untuk rute / kabin ini.',
			'error'
		)
	} else if (alertErr == 'kurang') {
		swal.fire(
			'Pemberitahuan',
			'Stok kursi pada kabin / rute sudah penuh mohon kurangi pemesanan kursi untuk rute tersebut.',
			'error'
		)
	}
	jQuery('#tanggal').datetimepicker({
		i18n: {
			de: {
				months: [
					'Januar', 'Februar', 'März', 'April',
					'Mai', 'Juni', 'Juli', 'August',
					'September', 'Oktober', 'November', 'Dezember',
				],
				dayOfWeek: [
					"So.", "Mo", "Di", "Mi",
					"Do", "Fr", "Sa.",
				]
			}
		},
		timepicker: false,
		format: 'd-m-Y'
	});

	$('#tanggal').click(function (e) {
		$('#rute').val('');
		$('#jam').val('');
		$('#kursi').val('');
		$('#dateTime').slideUp();

	});

	// rute on change
	$('#rute').change(function (e) {

		e.preventDefault();
		$('#tanggal').removeClass('is-invalid');

		const value = $(this).val();
		const tanggal = $('#tanggal').val();
		const hari = get_day(tanggal);

		const arrayDate = tanggal.split("-");

		var DateNow = new Date();
		const DateSelect = new Date(parseInt(arrayDate[2]), parseInt(arrayDate[1]), parseInt(arrayDate[0]));
		if (DateSelect < DateNow) {
			Swal.fire(
				'Tanggal Terlewat !',
				'Tanggal ' + tanggal + ' telah terlewat',
				'error'
			)
		} else {
			// CEK TANGGAL kosong atau tidak
			if (tanggal) {
				// CEK rute kosong atau idak
				if (value) {

					// cek jadwal tersedia atau todak
					$('#loader2').removeClass('d-none');
					$.ajax({
						type: "post",
						url: base_url + 'user/Boking/cek_jadwal',
						data: {
							id: value,
							hari: hari
						},
						dataType: "json",
						success: function (response) {
							$('#loader2').addClass('d-none');
							if (response.success == true) {
								if (response.data == null) {
									Swal.fire(
										'Jadwal tidak tersedia !',
										'Pelayaran pada tanggal ' + tanggal + ' tidak tersedia',
										'error'
									)
									$('#dateTime').slideUp();
								} else {
									$('#dateTime').slideDown();
									$('#tanggal').val(tanggal + ' ' + response.data.jam);
									$('#kapal').html(`<option value="` + response.data.id_kapal + `">` + response.data.nama + `</option>`);
								}
							} else if (response.success == false) {
								if (response.data.rute) {
									$('#rute').addClass('is-invalid');
									$('#errorRute').html(response.data.rute);
								}
							}
						}
					});
				} else {
					$('#dateTime').slideUp();
				}
			} else {
				$('#tanggal').addClass('is-invalid');
				$('#errorTanggal').html('Masukkan tanggal');
				$(this).val('');
			}
		}

	});

	// function change kabin
	$('#kabin').change(function (e) {
		e.preventDefault();
		$('#usia').val('');
		$('#kursi').html(`<option value="">Pilih kursi</option>`);
		$('#tarif').html('');
	});

	// function change usia
	$('#usia').change(function (e) {
		e.preventDefault();
		$('#kabin').removeClass('is-invalid');
		$('#kursi').html(`<option value="">Pilih kursi</option>`);
		const kabin = $('#kabin').val();
		const usia = $(this).val()
		const tgl = $('#tanggal').val();
		const id_rute = $('#rute').val();
		const kapal = $('#kapal').val();
		if (kabin) {
			if (usia) {
				$('#loader2').removeClass('d-none');
				$.ajax({
					type: "post",
					url: "user/Boking/cek_kursi",
					data: {
						'id': id_rute,
						'tgl': tgl,
						'kabin': kabin,
						'usia': usia,
						'kapal': kapal
					},
					dataType: "json",
					success: function (response) {
						$('#loader2').addClass('d-none');
						if (response.success == true) {
							if (response.data == null) {
								$('#dateTime').slideUp();
								$('#rute').val('');
								swal.fire(
									'Kursi penuh!',
									'Mohon maaf kursi sudah penuh untuk tanggal ' + tgl + ' pada tersebut',
									'error'
								);
								$('#usia').val('');
								$('#kabin').val('');
								$('#kursi').html(`<option value="">Pilih kursi</option>`);
								$('#tarif').html('');
							} else {
								$('#tarif').html('Rp.' + response.tarif);
								$.each(response.data, function (i, v) {
									$('#kursi').append(`<option value="` + v.id_kursi + `">` + v.kursi + `</option>`);
								});
							}
						} else if (response.success == false) {
							swal.fire(
								'Error !!',
								response.rute,
								'error'
							)
						}
					}
				});
			}

		} else {
			$('#usia').val('');
			$('#kabin').addClass('is-invalid');
			$('#errorKabin').html('this kabin is required');
		}
	});

	$('#formBoking').submit(function (e) {
		e.preventDefault();
		$('.form_control').removeClass('is-invalid');
		$('#loader2').removeClass('d-none');
		const value = $(this);

		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				$('#loader2').addClass('d-none');

				if (response.success == true) {
					if (response.kursi == 'kosong') {
						Swal.fire(
							'Error!',
							'Kursi sudah penuh.',
							'error'
						)
					}
					Swal.fire(
						'Berhasil!',
						'Terimakasih telah menggunakan jasa kami.',
						'success'
					)
					window.location.replace(base_url + 'invoice/' + response.no_faktur);
				} else if (response.success == false) {
					if (resposne.id_rute) {
						$('#id_rute').addClass('is-invalid');
						$('#errorRute').html(response.id_rute);
					}
					if (resposne.id_kapal) {
						$('#kapal').addClass('is-invalid');
						$('#errorKapal').html(response.id_kapal);
					}
					if (resposne.usia) {
						$('#usia').addClass('is-invalid');
						$('#errorUsia').html(response.usia);
					}
					if (resposne.tgl) {
						$('#tanggal').addClass('is-invalid');
						$('#errorTanggal').html(response.id_kapal);
					}
				}
			}
		});
	});

	function get_day(tanggal) {
		const string = tanggal.split('-');
		convertTanggal = string[2] + '-' + string[1] + '-' + string[0];

		var myDays = ['minggu', 'senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu'];
		var date = new Date(convertTanggal);
		var thisDay = date.getDay(),

			thisDay = myDays[thisDay];

		return thisDay;

	}
});
