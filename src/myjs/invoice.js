$('#batalBoking').click(function (e) {
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-success text-white',
			cancelButton: 'btn btn-danger text-white mr-2'
		},
		buttonsStyling: false
	})

	swalWithBootstrapButtons.fire({
		title: 'Yakin Batalkan?',
		text: "Apakah anda yakin akan membatalkan tiket tersebut",
		icon: 'error',
		showCancelButton: true,
		confirmButtonText: 'Tidak!',
		cancelButtonText: 'ya, batalkan',
		reverseButtons: true
	}).then((result) => {
		if (result.value) {
			return
		} else if (result.dismiss === Swal.DismissReason.cancel) {
			const id = $('#idInvoiceBoking').html();
			$('#loader2').removeClass('d-none');
			$.ajax({
				type: "POST",
				url: base_url + 'user/Daftar_tiket/batal_boking',
				data: {
					id: id
				},
				dataType: "json",
				success: function (response) {
					$('#loader2').addClass('d-none');
					if (response.success == true) {
						Swal.fire(
							'Berhasil.',
							'Berhasil Membatalkan Pemesanan tiket',
							'success',
						);
						$('#status').html('Pemesanan tiket di batalkan');
						$('#containerKonfirmasi').html('');

						if (response.jumlah_tiket > 0) {
							$('.jumlah-tiket').removeClass('d-none');
							$('.jumlah-tiket').html(response.jumlah_tiket);
						} else if (response.jumlah_tiket < 1) {

							$('.jumlah-tiket').addClass('d-none');
						}
					} else if (response.success == false) {
						Swal.fire(
							'Error!!.',
							'Pastikan Untuk membatalkan lewat Tombol Batal yang tersedia',
							'success',
						);
					}
				}
			});
		}
	})


});
