$(document).ready(function () {
	let kursi = []
	// function show pilih kursi
	$(document).on('click', '#btnTambahKursi', function (e) {
		e.preventDefault();
		$('.data').slideUp();
		$('#selectKursi').slideDown();
	});

	$('#btnSaveKursi').click(function (e) {
		$('#listKursi').html('');
		kursi = []
		$.each($("input[name='id_kursi[]']:checked"), function () {
			kursi.push({
				'id_kursi': $(this).val(),
				'no_kursi': $(this).attr('no-kursi')
			});
		});
		if (kursi) {

			$.each(kursi, function (i, v) {

				$('#listKursi').append(`<li class="list-group-item justify-content-between align-items-center text-black">
				<div class="form-group">
					<input type="text" required name="nama[]" placeholder="Nama Penerima" required class="form-control">
				</div>
				<div class="form-group">
					<input type="number" required name="phone[]" placeholder="Phone" required class="form-control">
				</div>
				<div class="form-group d-flex justify-content-between">
					No: ` + v.no_kursi + `
					<select required name="usia[]" id="` + v.id_kursi + `" class="badge badge-primary kursi-boking badge-pill">
						<option value="">Pilih usia</option>
						<option value="dewasa">Dewasa</option>
						<option value="anak">Anak</option>
					</select>
				</div>
			</li>`);
			});
			$('#listKursi').append(`<li class="list-group-item text-center " id="btnTambahKursi" style="cursor: pointer;">Tambah kursi <i class="fa fa-plus-circle"></i></li>`);

		}
		$('.data').slideDown();
		$('#selectKursi').slideUp();
	});

	$('#btnCancel').click(function (e) {
		e.preventDefault();
		$('.data').slideDown();
		$('#selectKursi').slideUp();
	});

	$(document).on('change', '.kursi-boking', function () {
		const id_rute = $('#id_rute').val();
		const usia = $(this).val();
		let total = $('#total').val();
		total > 0 ? total = total : total = 0
		console.log(total)
		$.ajax({
			type: "post",
			url: base_url + "user/Pilih_kursi/cek_tarif",
			data: {
				id_rute: id_rute,
				usia: usia
			},
			dataType: "json",
			success: function (response) {
				if (response.success == true) {
					let last_total = parseInt(response.tarif) + parseInt(total);
					$('#viewTarif').html('Total : Rp.' + Number(last_total));
					$('#total').val(last_total);
				}
			}
		});

	});

	$('#formBoking').submit(function (e) {
		e.preventDefault();
		const value = $(this);
		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				if (response.success == false) {
					swal.fire(
						'Pemberitahuan',
						'Harap mengisi semua form dengan benar',
						'error'
					)
				} else {
					swal.fire(
						'Pemberitahuan',
						'Boking tiket berhasil',
						'success'
					)
					window.location.replace(base_url + 'invoice/' + response.no_faktur)
				}
			}
		});
	});
});
