$(document).ready(function () {
	$('#laporanHari').click(function (e) {
		e.preventDefault();
		$('#value').removeClass('is-invalid');
		$('#labelLaporan').html('Tanggal');
		$('#formLaporan').attr('action', base_url + 'laporan-hari');
		$('#value').attr('placeholder', 'Masukkan tanggal');
		$('#action').slideDown();
		jQuery('#value').datetimepicker({
			i18n: {
				de: {
					months: [
						'Januar', 'Februar', 'März', 'April',
						'Mai', 'Juni', 'Juli', 'August',
						'September', 'Oktober', 'November', 'Dezember',
					],
					dayOfWeek: [
						"So.", "Mo", "Di", "Mi",
						"Do", "Fr", "Sa.",
					]
				}
			},
			timepicker: false,
			format: 'd F Y'
		});
	});

	$('#laporanBulan').click(function (e) {
		e.preventDefault();
		$('#labelLaporan').html('Bulan');
		$('#formLaporan').attr('action', base_url + 'laporan-bulan');
		$('#value').attr('placeholder', 'Masukkan bulan');
		$('#action').slideDown();
		$('#value').datepicker({
			format: "mm-yyyy",
			viewMode: "months",
			minViewMode: "months"
		});
	});

	$('.cancel').click(function () {
		$('#action').slideUp();
	});

	$('#formLaporan').submit(function (e) {
		let value = $('#value').val();
		if (!value) {
			e.preventDefault();
			$('#value').addClass('is-invalid');
			$('#errorValue').html('Filend this required');
		}
	});

	$('#closeAlert').click(function () {
		$('#alertAction').slideUp();
	})
});
