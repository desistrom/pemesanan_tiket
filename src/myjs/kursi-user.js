jQuery('#tgl').datetimepicker({
	i18n: {
		de: {
			months: [
				'Januar', 'Februar', 'März', 'April',
				'Mai', 'Juni', 'Juli', 'August',
				'September', 'Oktober', 'November', 'Dezember',
			],
			dayOfWeek: [
				"So.", "Mo", "Di", "Mi",
				"Do", "Fr", "Sa.",
			]
		}
	},
	timepicker: false,
	format: 'd-m-Y'
});

$('#btnSetTgl').click(function (e) {
	e.preventDefault();
	$('#modalKursi').modal('show');
});
