$(document).ready(function () {
	$('#formRegister').submit(function (e) {
		e.preventDefault();
		$('#loader2').removeClass('d-none');
		let value = $(this);
		$('.text-danger').html('');
		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				$('#loader2').addClass('d-none')
				if (response.success == false) {
					if (response.nama) {
						$('#errorNama').html(response.nama);
					}
					if (response.email) {
						$('#errorEmail').html(response.email);
					}
					if (response.password) {
						$('#errorPassword').html(response.password);
					}
					if (response.password2) {
						$('#errorPassword2').html(response.password2);
					}
					if (response.alamat) {
						$('#errorAlamat').html(response.alamat);
					}
				} else if (response.success == true) {
					window.location.replace(base_url + 'Login');
				}
			}
		});
	});
});
