$(document).ready(function () {

	var datatable;
	datatable = $('#table').DataTable({
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax": {
			"url": base_url + 'admin/Boking/show',
			"type": "POST"
		},


		"columnDefs": [{
			"targets": [0],
			"orderable": false,
		}, ],
	});

	$('#searchDatatable').on('keyup', function () { //Search using custom input box
		datatable.search(this.value).draw();
	});

	$(document).on('click', '.btnUpdateStatus', function () {
		const id = $(this).attr('id');
		$('#updateStatusHeadler').slideDown();
		$('#data').slideUp();
		$('#idUpdateStatusBoking').val(id);
		$('#bokingStatus').val('');
	});

	$('#btnCloseForm').click(function (e) {
		e.preventDefault();
		$('#updateStatusHeadler').slideUp();
		$('#data').slideDown();
	});

	$('#updateStatusBoking').submit(function (e) {
		e.preventDefault();
		$('#bokingStatus').removeClass('is-invalid');
		$('#loader2').removeClass('d-none');
		const value = $(this);
		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				$('#loader2').addClass('d-none');

				if (response.success == false) {
					if (response.status) {
						$('#bokingStatus').addClass('is-invalid');
						$('#bokingStatusError').html(response.status);
					}
				} else {
					datatable.ajax.reload();
					$('#updateStatusHeadler').slideUp();
					$('#data').slideDown();
					$('#updateStatusBoking')[0].reset();
				}
			}
		});
	});

});
