$(document).ready(function () {
	$('#btnInsert').click(function (e) {
		e.preventDefault();
		$('#idRute option').removeAttr('selected');
		$('#formAction').attr('action', base_url + 'admin/Kota/insert');
		$('.form-control').removeClass('is-invalid');
		$('#data').slideUp();
		$(this).slideUp();
		$('#action').slideDown();
		$('#btnView').slideDown();
		$('#formAction')[0].reset();
	});

	$('#btnView').click(function (e) {
		e.preventDefault();
		$('#action').slideUp();
		$(this).slideUp();
		$('#data').slideDown();
		$('#btnInsert').slideDown();
	});

	$(document).on('click', '.edit', function (e) {
		e.preventDefault();
		const id = $(this).attr('id');

		$.ajax({
			type: "post",
			url: base_url + 'admin/Kota/show_data',
			data: {
				id: id
			},
			dataType: "json",
			success: function (response) {
				const value = response.data;

				if (response.success == false) {
					$('#alert').addClass('alert-danger');
					$('#alert').html('data tidak di temukan');
				} else if (response.success == true) {
					$('#formAction').attr('action', base_url + 'admin/Kota/update');
					$('#idKota').val(value.id_kota);
					$('.form-control').removeClass('is-invalid');
					$('#data').slideUp();
					$('#btnInsert').slideUp();
					$('#action').slideDown();
					$('#btnView').slideDown();

					$('#kota').val(value.kota);
					$('#jarak').val(value.jarak);
					$('#posisi').val(value.posisi);

					$('#idRute option').filter(function () {
						if (this.value == value.id_rute) {
							$(this).attr('selected', 'selected');
						}
					});
				}

			}
		});
	});

	var datatable;
	datatable = $('#table').DataTable({
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax": {
			"url": base_url + 'admin/Kota/show',
			"type": "POST"
		},


		"columnDefs": [{
			"targets": [0],
			"orderable": false,
		}, ],
	});

	$('#searchDatatable').on('keyup', function () { //Search using custom input box
		datatable.search(this.value).draw();
	});

	$('#closeAlert').click(function (e) {
		e.preventDefault();
		$('#alertAction').addClass('d-none');
	});

	$('#formAction').submit(function (e) {
		e.preventDefault();

		let value = $(this);
		$('.form-control').removeClass('is-invalid');
		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				if (response.success == false) {
					if (response.id_rute) {
						$('#idRute').addClass('is-invalid');
						$('#errorIdRute').html(response.id_rute);
					}
					if (response.kota) {
						$('#kota').addClass('is-invalid');
						$('#errorKota').html(response.kota);
					}
					if (response.id_rute) {
						$('#idRute').addClass('is-invalid');
						$('#errorIdRute').html(response.id_rute);
					}
					if (response.jarak) {
						$('#jarak').addClass('is-invalid');
						$('#errorJarak').html(response.jarak);
					}
					if (response.posisi) {
						$('#posisi').addClass('is-invalid');
						$('#errorPosisi').html(response.posisi);
					}
				} else if (response.success == true) {
					datatable.ajax.reload();
					$('#alert').html(response.alert);
					$('#alertAction').removeClass('d-none');
					$('#btnView').slideUp();
					$('#btnInsert').slideDown();
					$('#action').slideUp();
					$('#data').slideDown();
				}
			}
		});
	});
});
