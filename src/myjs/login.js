$(document).ready(function () {
	let getUrl = window.location.search;
	let urlParams = new URLSearchParams(getUrl);
	let boking = urlParams.get('boking')
	$('#formLogin').submit(function (e) {
		$('#loader2').removeClass('d-none');
		e.preventDefault();
		$('#alert').slideUp();
		let value = $(this);
		$('.text-danger').html('');

		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				$('#loader2').addClass('d-none');
				if (response.success == false) {
					if (response.email) {
						$('#errorEmail').html(response.email);
					}
					if (response.password) {
						$('#errorPassword').html(response.password);
					}
				} else if (response.success == true) {
					$('#alert').html('<div class="text-center alert alert-success col-sm-12">Login Berhasil</div>');
					$('#alert').slideDown();
					if (boking) {
						window.location.replace(base_url + 'boking');
					} else {
						window.location.replace(base_url);
					}
				}
			}
		});
	});
});
