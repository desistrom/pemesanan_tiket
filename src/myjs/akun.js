$(document).ready(function () {

	jQuery('#tanggal').datetimepicker({
		i18n: {
			de: {
				months: [
					'Januar', 'Februar', 'März', 'April',
					'Mai', 'Juni', 'Juli', 'August',
					'September', 'Oktober', 'November', 'Dezember',
				],
				dayOfWeek: [
					"So.", "Mo", "Di", "Mi",
					"Do", "Fr", "Sa.",
				]
			}
		},
		timepicker: false,
		format: 'd-m-Y'
	});




	$('#formAkun').submit(function (e) {
		e.preventDefault();
		$('.form_control').removeClass('is-invalid');
		// $('#loader2').removeClass('d-none');
		const value = $(this);

		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				$('#loader2').addClass('d-none');

				if (response.success == true) {
					Swal.fire(
						'Berhasil!',
						'Berhasil update user.',
						'success'
					)
					window.location.reload();
				} else if (response.success == false) {
					if (response.nama) {
						$('#nama').addClass('is-invalid');
						$('#errorNama').html(response.nama);
					}
					if (response.alamat) {
						$('#alamat').addClass('is-invalid');
						$('#errorAlamat').html(response.alamat);
					}
					console.log(response.alert)
					if (response.alert) {
						Swal.fire(
							'Error!',
							response.alert,
							'error'
						)
					}
				}
			}
		});
	});

});
