$(document).ready(function () {
	$('#formInvoice').submit(function (e) {
		e.preventDefault();
		const value = $(this);
		$('#loader2').removeClass('d-none');
		const no_faktur = $('#noFaktur').val();
		$('.form-control').removeClass('is-invalid');
		$('#errorNoFaktur').addClass('d-none');
		$.ajax({
			type: "POST",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				$('#loader2').addClass('d-none');

				if (response.success == true) {
					if (response.data > 0) {
						window.location.replace(base_url + 'invoice/' + no_faktur)
					} else {
						swal.fire(
							'Error',
							'Nomor faktur ' + no_faktur + ' tidak tersedia',
							'error'
						)
					}
				} else if (response.success == false) {
					if (response.no_faktur) {
						$('#errorNoFaktur').html(response.no_faktur);
						$('#errorNoFaktur').removeClass('d-none');
					}
				}
			}
		});
	});
});
