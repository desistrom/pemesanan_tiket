$(document).ready(function () {


	$('#btnInsert').click(function (e) {
		e.preventDefault();
		$('#idRute option').removeAttr('selected');
		$('#hari option').removeAttr('selected');
		$('#formAction')[0].reset();
		$('#formAction').attr('action', base_url + 'admin/Jadwal/insert');
		$('.form-control').removeClass('is-invalid');
		$('#data').slideUp();
		$(this).slideUp();
		$('#action').slideDown();
		$('#btnView').slideDown();
	});

	$('#btnView').click(function (e) {
		e.preventDefault();
		$('#action').slideUp();
		$(this).slideUp();
		$('#data').slideDown();
		$('#btnInsert').slideDown();
	});

	$(document).on('click', '.edit', function (e) {
		e.preventDefault();
		const id = $(this).attr('id');
		$('#idRute option').removeAttr('selected');
		$('#hari option').removeAttr('selected');

		$.ajax({
			type: "post",
			url: base_url + 'admin/Jadwal/show_data',
			data: {
				id: id
			},
			dataType: "json",
			success: function (response) {
				const value = response.data;
				console.log(value);
				if (response.success == false) {
					$('#alert').addClass('alert-danger');
					$('#alert').html('data tidak di temukan');
				} else if (response.success == true) {
					$('#formAction').attr('action', base_url + 'admin/Jadwal/update');
					$('.form-control').removeClass('is-invalid');
					$('#data').slideUp();
					$('#btnInsert').slideUp();
					$('#action').slideDown();
					$('#btnView').slideDown();
					$('#idJadwal').val(value.id_jadwal);
					$('#idKapal').val(value.id_kapal);
					$('#hari').val(value.hari);
					$('#jam').val(value.jam);
					$('#idRute').val(value.id_rute);
				}

			}
		});
	});

	var datatable;
	datatable = $('#table').DataTable({
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax": {
			"url": base_url + 'admin/Jadwal/show',
			"type": "POST"
		},


		"columnDefs": [{
			"targets": [0],
			"orderable": false,
		}, ],
	});

	$('#searchDatatable').on('keyup', function () { //Search using custom input box
		datatable.search(this.value).draw();
	});

	$('#closeAlert').click(function (e) {
		e.preventDefault();
		$('#alertAction').addClass('d-none');
	});

	$('#formAction').submit(function (e) {
		e.preventDefault();

		let value = $(this);
		$('.form-control').removeClass('is-invalid');
		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				if (response.success == false) {
					if (response.jadwal) {
						$('#jadwal').addClass('is-invalid');
						$('#errorJadwal').html(response.jadwal);
					}

					if (response.hari) {
						$('#hari').addClass('is-invalid');
						$('#errorHari').html(response.hari);
					}

					if (response.jam) {
						$('#jam').addClass('is-invalid');
						$('#errorJam').html(response.jam);
					}

					if (response.id_kapal) {
						$('#kapal').addClass('is-invalid');
						$('#errorIdKapal').html(response.id_kapal);
					}

					if (response.id_rute) {
						$('#idRute').addClass('is-invalid');
						$('#errorIdRute').html(response.id_rute);
					}

				} else if (response.success == true) {
					datatable.ajax.reload();
					$('#alert').html(response.alert);
					$('#alertAction').removeClass('d-none');
					$('#btnView').slideUp();
					$('#btnInsert').slideDown();
					$('#action').slideUp();
					$('#data').slideDown();
				}
			}
		});
	});
});
