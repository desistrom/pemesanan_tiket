$(document).ready(function () {
	$('#btnInsert').click(function (e) {
		e.preventDefault();
		$('#status option').removeAttr('selected');
		$('#formAction')[0].reset();
		$('#formAction').attr('action', base_url + 'admin/Kapal/insert');
		$('.form-control').removeClass('is-invalid');
		$('#data').slideUp();
		$(this).slideUp();
		$('#action').slideDown();
		$('#btnView').slideDown();
	});

	$('#btnView').click(function (e) {
		e.preventDefault();
		$('#action').slideUp();
		$('#btnInsert').slideDown();
		$(this).slideUp();
		$('#data').slideDown();
	});

	$(document).on('click', '.edit', function (e) {
		e.preventDefault();
		const id = $(this).attr('id');

		$.ajax({
			type: "post",
			url: base_url + 'admin/Kapal/show_data',
			data: {
				id: id
			},
			dataType: "json",
			success: function (response) {
				const value = response.data;

				if (response.success == false) {
					$('#alert').addClass('alert-danger');
					$('#alert').html('data tidak di temukan');
				} else if (response.success == true) {
					$('#formAction').attr('action', base_url + 'admin/Kapal/update');
					$('#idKapal').val(value.id_kapal);
					$('.form-control').removeClass('is-invalid');
					$('#data').slideUp();
					$('#btnInsert').slideUp();
					$('#action').slideDown();
					$('#btnView').slideDown();
					$('#kapal').val(value.nama);
					$('#status').val(value.status);
				}

			}
		});
	});

	var datatable;
	datatable = $('#table').DataTable({
		"processing": true,
		"serverSide": true,
		"order": [],

		"ajax": {
			"url": base_url + 'admin/Kapal/show',
			"type": "POST"
		},


		"columnDefs": [{
			"targets": [0],
			"orderable": false,
		}, ],
	});

	$('#searchDatatable').on('keyup', function () { //Search using custom input box
		datatable.search(this.value).draw();
	});
	// mengahapus alert
	$('#closeAlert').click(function (e) {
		e.preventDefault();
		$('#alertAction').addClass('d-none');
	});

	$('#formAction').submit(function (e) {
		e.preventDefault();

		let value = $(this);
		$('.form-control').removeClass('is-invalid');
		$.ajax({
			type: "post",
			url: value.attr('action'),
			data: value.serialize(),
			dataType: "json",
			success: function (response) {
				if (response.success == false) {
					if (response.nama) {
						$('#kapal').addClass('is-invalid');
						$('#errorKapal').html(response.nama);
					}
					if (response.status) {
						$('#status').addClass('is-invalid');
						$('#errorStatus').html(response.status);
					}
				} else if (response.success == true) {
					datatable.ajax.reload();
					$('#alert').html(response.alert);
					$('#alertAction').removeClass('d-none');
					$('#btnView').slideUp();
					$('#btnInsert').slideDown();
					$('#action').slideUp();
					$('#data').slideDown();
				}
			}
		});
	});
});
